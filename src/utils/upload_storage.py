from google.cloud import bigquery, storage
from google.oauth2 import service_account 
import google.auth
import google.auth.transport.requests
creds, project = google.auth.default()

# creds.valid is False, and creds.token is None
# Need to refresh credentials to populate those
auth_req = google.auth.transport.requests.Request()
creds.refresh(auth_req)

BIGQUERY_KEY_PATH = "/home/jupyter/src/creds.json"
key_path = BIGQUERY_KEY_PATH
credentials = service_account.Credentials.from_service_account_file(
key_path,
scopes=["https://www.googleapis.com/auth/cloud-platform"],
)

storage_client = storage.Client(credentials=creds, project=credentials.project_id)    
bucket_name='kohls-bda-mkt-prd-marketing-usr'
bucket = storage_client.bucket(bucket_name)

for filename in [
    "/home/jupyter/data/train_v5.json", 
    "/home/jupyter/data/valid_v5.json",
    "/home/jupyter/data/test_v5.json",
    "/home/jupyter/data/neg_sample_v5.json",
    ]:
    print(filename)
    blob = bucket.blob(filename)
    blob.upload_from_filename(filename)

