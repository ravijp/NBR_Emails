"""ensure data folder is present"""

# -*- coding:utf-8 -*-
__author__ = 'RP'

import os
import pickle
import pandas as pd
from tqdm import tqdm
from sklearn.model_selection import train_test_split
from google.cloud import bigquery

# client = bigquery.Client()
data_version = "v2"

def download_data(save_flag:bool=False, save_file_path:str=f"../data/prep_data_{data_version}.parquet"):
    """Download data from bigquery returns dataframe
    Args:
        save_flag: bool 
        save_file
    Returns:
        Train Valid Test 
    """
    def _get_lists(x):
        res, res_c = [],[]
        for d in x:
            res.append(list(d.values())[0].tolist())
        return res
    
    client = bigquery.Client()
    sql = """
    select * except(rnd_val)
    from `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_v3`
    WHERE rnd_val < 0.3
    """
    
    df = client.query(sql).to_dataframe()
    print(f"Downloaded data from v3 {df.shape}")
    
    df['deps'] = df.dept_nbrs.apply(_get_lists)
    df['tot_b'] = df.deps.apply(len)
    df = df.rename(columns={'cust_id':'userID', 'deps':'baskets', 'tot_b':'num_baskets'})
    if save_flag:
        df[['userID', 'baskets', 'num_baskets']].to_parquet(save_file_path)
    return df
    

def split_data(df, save_flag:bool=True):
    """
    Split_data.
    Args:
        input_dataframe:
    Returns:
        Train Valid Test

    0-11 timestamp items are used for training 12 timestamp items are used for testing
    The items at each moment are called a basket

    Example:
    userID  baskets   num_baskets
    1   [[2, 5],[3, 7]]  2
    """
    train_valid, test = [], []
    for i, r in df.iterrows():
        train_valid.append(
            [
                r['userID'], r['baskets'][0:-1], r['num_baskets'] - 1
            ]
        )
    train_valid = pd.DataFrame(train_valid, columns=['userID', 'baskets', 'num_baskets'])    
    train, valid = train_test_split(train_valid, test_size=0.1, random_state=24)
    
    for i, r in df.iterrows():
        test.append(
            [
                r['userID'], r['baskets'][-1]
            ]
        )
    test = pd.DataFrame(test, columns=["userID", "baskets"])

    if save_flag:
        train.to_json(f'../data/train_{data_version}.json', orient='records', lines=True)
        valid.to_json(f'../data/valid_{data_version}.json', orient='records', lines=True)
        test.to_json(f'../data/test_{data_version}.json', orient='records', lines=True)
    print(f"train: {train.shape}, valid: {valid.shape}, test:{test.shape}")
    return train, valid, test


def negative_sample(pickle_file:str=f"../data/neg_sample_{data_version}.pickle"):
    """
    Create negative sample.
    All items minus the items that the user interacted with(only training data, not including last bucket) is a negative sample 
    Args:
        pickle_file: storage file
    Returns:
        Dictionary storage (key: values) -> (userID: list of negative samples for userID)
    """
    client = bigquery.Client()
    sql = """
    select cust_id, neg_deps from `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_v4`
    where cust_id in (
        select distinct cust_id 
        from `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_v3`
        WHERE rnd_val < 0.3
    )
    """
    # import random
    # neg_samples = neg_samples.assign(neg_deps_smpl = neg_samples.neg_deps.apply(lambda x: random.sample(x, 200)))
    df = client.query(sql).to_dataframe()
    print(f"Downloaded negative samples from v4 {df.shape}")
    # df['neg_deps'] = df['neg_deps'].map(set)
    df = df.rename(columns={"cust_id":"userID"})
    # df = df.set_index('userID')['neg_deps'].to_dict()
    # with open(pickle_file, 'wb') as handle:
    #     pickle.dump(df, handle, protocol=pickle.HIGHEST_PROTOCOL)
    df.to_json(pickle_file.replace('.pickle','.json'), orient='records', lines=True)    
    
if __name__ == '__main__':
    print(f"Running preprocessing and saving versions as {data_version} in ../data/ folder")
    assert os.path.isdir("../data"), "Please create a data folder in parent directory"
    df = download_data(save_flag=True) # download data
    train, valid, test = split_data(df) # split data

    negative_sample() #save negative samples in pickle file
    print("Completed!")


# import subprocess, os;os.getcwd()
# os.getcwd()
# # '/home/jupyter/src'
# DIR = os.path.join(os.getcwd(), 'utils/data_preprocess.py')
# subprocess.call(['python', DIR])
