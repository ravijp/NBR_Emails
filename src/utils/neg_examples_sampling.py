import os, sys; sys.path.insert(1, os.path.join(sys.path[0], '../data'))

sys.path.extend([os.path.join(os.getcwd(), 'utils')])

from utils import data_helpers as dh

class Config(object):
    def __init__(self):
        self.TRAININGSET_DIR = '../data/train_v5.json'
        self.VALIDATIONSET_DIR = '../data/valid_v5.json'
        self.TESTSET_DIR = '../data/test_v5.json'
        self.NEG_SAMPLES = '../data/neg_sample_v5.pickle'
        self.MODEL_DIR = 'runs/'
        self.cuda = False
        self.clip = 10
        self.epochs = 2
        self.batch_size = 256
        self.seq_len = 19
        self.learning_rate = 0.01  # Initial Learning Rate
        self.log_interval = 1  # num of batches between two logging
        self.basket_pool_type = 'max'  # ['avg', 'max']
        self.rnn_type = 'RNN_TANH'  # ['RNN_TANH', 'RNN_RELU', 'LSTM', 'GRU']
        self.rnn_layer_num = 2
        self.dropout = 0.5
        self.num_product = 1118+1  # number of items, used to define Embedding Layer
        self.embedding_dim = 3  # commodity representation dimension, used to define Embedding Layer
        self.neg_num = 300  # number of negative samples
        self.top_k = 10  # Top K value
Config().NEG_SAMPLES

%%time
neg_samples = dh.load_data(Config().NEG_SAMPLES.replace('.pickle', '.json'))


import random

neg_samples = neg_samples.assign(neg_deps_smpl = neg_samples.neg_deps.apply(lambda x: random.sample(x, 300)))


neg_samples_v5=neg_samples[['userID','neg_deps_smpl']]
neg_samples_v5.rename(columns={'neg_deps_smpl':'neg_deps'},inplace=True)

# neg_samples_v5.to_pickle(f"../data/neg_sample_v5.pickle")
neg_samples_v5.to_json(f'../data/neg_sample_v5_sample.json', orient='records', lines=True)


"""Upload the file to storage
"""
from google.cloud import storage
from google.oauth2 import service_account 
import google.auth
import google.auth.transport.requests
creds, project = google.auth.default()
# creds.valid is False, and creds.token is None
# Need to refresh credentials to populate those
auth_req = google.auth.transport.requests.Request()
creds.refresh(auth_req)
BIGQUERY_KEY_PATH = "/home/jupyter/src/creds.json"
key_path = BIGQUERY_KEY_PATH
credentials = service_account.Credentials.from_service_account_file(
key_path,
scopes=["https://www.googleapis.com/auth/cloud-platform"],
)
storage_client = storage.Client(credentials=creds, project=credentials.project_id)    
bucket_name='kohls-bda-mkt-prd-marketing-usr'
bucket = storage_client.bucket(bucket_name)
for filename in [
    # "/home/jupyter/data/train_v5.json", 
    # "/home/jupyter/data/valid_v5.json",
    # "/home/jupyter/data/test_v5.json",
    # "/home/jupyter/data/neg_sample_v5.json",
    "/home/jupyter/data/neg_sample_v5_sample.json"
    ]:
    print(filename)
    blob = bucket.blob(filename)
    blob.upload_from_filename(filename)