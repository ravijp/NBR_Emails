# -*- coding:utf-8 -*-
__author__ = 'RP'

# RNN_TANH_30_negexample
class Config(object):
    def __init__(self):
        self.TRAININGSET_DIR = '../data/train_v4.json'
        self.VALIDATIONSET_DIR = '../data/valid_v4.json'
        self.TESTSET_DIR = '../data/test_v4.json'
        self.NEG_SAMPLES = '../data/neg_sample_v4.pickle'
        self.MODEL_DIR = 'runs/'
        self.cuda = False
        self.clip = 10
        self.epochs = 2
        self.batch_size = 256
        self.seq_len = 19
        self.learning_rate = 0.01  # Initial Learning Rate
        self.log_interval = 1  # num of batches between two logging
        self.basket_pool_type = 'max'  # ['avg', 'max']
        self.rnn_type = 'LSTM'  # ['RNN_TANH', 'RNN_RELU', 'LSTM', 'GRU']
        self.rnn_layer_num = 2
        self.dropout = 0.5
        self.num_product = 139+1  # number of items, used to define Embedding Layer
        self.embedding_dim = 3  # commodity representation dimension, used to define Embedding Layer
        self.neg_num = 35  # number of negative samples
        self.top_k = 10  # Top K value

# class Config(object):
#     def __init__(self):
#         self.TRAININGSET_DIR = '../data/Train.json'
#         self.VALIDATIONSET_DIR = '../data/Validation.json'
#         self.TESTSET_DIR = '../data/Test.json'
#         self.NEG_SAMPLES = '../data/neg_sample.pickle'
#         self.MODEL_DIR = 'runs/'
#         self.cuda = False
#         self.clip = 10
#         self.epochs = 2
#         self.batch_size = 256
#         self.seq_len = 12
#         self.learning_rate = 0.01  # Initial Learning Rate
#         self.log_interval = 1  # num of batches between two logging
#         self.basket_pool_type = 'max'  # ['avg', 'max']
#         self.rnn_type = 'LSTM'  # ['RNN_TANH', 'RNN_RELU', 'LSTM', 'GRU']
#         self.rnn_layer_num = 2
#         self.dropout = 0.5
#         self.num_product = 139+1  # number of items, used to define Embedding Layer
#         self.embedding_dim = 3  # commodity representation dimension, used to define Embedding Layer
#         self.neg_num = 500  # number of negative samples
#         self.top_k = 10  # Top K value
