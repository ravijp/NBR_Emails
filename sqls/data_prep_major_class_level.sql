-- Take most updated dept_nbr and dept_nm
create or replace table `kohls-bda-mkt-prd.dp_marketing_sandbox.bk_email_recomm_v0_other` as
select *, 
first_value(dept_nm) over(partition by dma_nbr, dma_nm,modified_dept_nbr order by max_dmnd_dte_dept_nm desc) as modified_dept_nm
from
	(select *,
	first_value(dept_nbr) over(partition by dma_nbr,dma_nm,dept_nm order by max_dmnd_dte_dept_nbr desc) as modified_dept_nbr
	from
		(select 
		dma_nbr,
		dma_nm, 
		dept_nbr,
		min_dmnd_dte_dept_nbr,
		max_dmnd_dte_dept_nbr,
		dept_nm, 
		min_dmnd_dte_dept_nm,
		max_dmnd_dte_dept_nm,
		maj_cl_nbr,
		min_dmnd_dte_maj_cl_nbr,
		max_dmnd_dte_maj_cl_nbr,
		maj_cl_nm, 
		min_dmnd_dte_maj_cl_nm,
		max_dmnd_dte_maj_cl_nm,
		count(*) as trans, count(distinct cust_id) as custs, sum(ffld_net_chrgd_amt) as amount
		from
			(select  
			*,
			min(dmnd_dte) over(partition by dma_nbr,dma_nm,dept_nbr) as min_dmnd_dte_dept_nbr,
			min(dmnd_dte) over(partition by dma_nbr,dma_nm,dept_nbr,maj_cl_nbr) as min_dmnd_dte_maj_cl_nbr,
			
			min(dmnd_dte) over(partition by dma_nbr,dma_nm,dept_nm) as min_dmnd_dte_dept_nm,
			min(dmnd_dte) over(partition by dma_nbr,dma_nm,dept_nm,maj_cl_nm) as min_dmnd_dte_maj_cl_nm,
			
			max(dmnd_dte) over(partition by dma_nbr,dma_nm,dept_nbr) as max_dmnd_dte_dept_nbr,
			max(dmnd_dte) over(partition by dma_nbr,dma_nm,dept_nbr,maj_cl_nbr) as max_dmnd_dte_maj_cl_nbr,
			
			max(dmnd_dte) over(partition by dma_nbr,dma_nm,dept_nm) as max_dmnd_dte_dept_nm,
			max(dmnd_dte) over(partition by dma_nbr,dma_nm,dept_nm,maj_cl_nm) as max_dmnd_dte_maj_cl_nm
			
			from `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_v0_other`)
		group by 1, 2,3,4, 5, 6,7,8, 9, 10, 11, 12, 13, 14)
	);


-- Take most updated maj_cl_nbr and maj_cl_nm and create modified_nm and modified_nbr
create or replace table `kohls-bda-mkt-prd.dp_marketing_sandbox.bk_email_recomm_v1_other` as
	select 
		*,
		concat(dma_nbr, '_', modified_dept_nbr, '_', modified_maj_cl_nbr) as modified_nbr,
		concat(dma_nm, '_', modified_dept_nm, '_', modified_maj_cl_nm) as modified_nm,
		concat(dma_nbr, '_', dept_nbr, '_', maj_cl_nbr) as not_modified_nbr,
		concat(dma_nm, '_', dept_nm, '_', maj_cl_nm) as not_modified_nm
	from
		(
		select 
			*,
			first_value(maj_cl_nm) over
			(partition by dma_nbr, dma_nm,modified_dept_nbr, modified_dept_nm, modified_maj_cl_nbr order by max_dmnd_dte_maj_cl_nm desc) as modified_maj_cl_nm
		from
			(select 
				*, 
				first_value(maj_cl_nbr) over
				(partition by dma_nbr,dma_nm,modified_dept_nbr, maj_cl_nm order by max_dmnd_dte_maj_cl_nbr desc) as modified_maj_cl_nbr
			from
				`kohls-bda-mkt-prd.dp_marketing_sandbox.bk_email_recomm_v0_other`
			)
		);

-- Create zids


-- Join
create or replace table `kohls-bda-mkt-prd.dp_marketing_sandbox.bk_email_recomm_v0` as
select 
	t1.*, 
	-- modified_dept_nbr, modified_dept_nm, modified_maj_cl_nbr,modified_maj_cl_nm
	t2.modified_dept_nbr,
	t2.modified_dept_nm,
	t2.modified_maj_cl_nbr,
	t2.modified_maj_cl_nm,
	t2.modified_nbr,
	t2.modified_nm,
	t3.zid as nbr_zid
from 
	`kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_v0_other` t1 
	left join
	(Select 			
		distinct 
		dma_nbr,dma_nm,
		dept_nbr,modified_dept_nbr,dept_nm, modified_dept_nm,
		maj_cl_nbr,modified_maj_cl_nbr, maj_cl_nm, modified_maj_cl_nm,
		modified_nbr, modified_nm
    from 
		`kohls-bda-mkt-prd.dp_marketing_sandbox.bk_email_recomm_v1_other`)t2
	on 
		t1.dma_nbr = t2.dma_nbr and 
		t1.dma_nm = t2.dma_nm and 
		t1.dept_nbr= t2.dept_nbr and 
		t1.dept_nm=t2.dept_nm and 
		t1.maj_cl_nbr=t2.maj_cl_nbr and 
		t1.maj_cl_nm=t2.maj_cl_nm
	right join 
	`kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_majcls_zid_mapping` t3 -- table with top 98% zids
	on t2.modified_nbr=t3.num_id and 
	t2.modified_nm=t3.nm_id;
-- done 

-- 
-- **********  Aggregating data at transactions level for each customer ********** 
-- 
	
create or replace table `kohls-bda-mkt-prd.dp_marketing_sandbox.bk_email_recomm_v1` as
select
    cust_id,
    mstr_trn_hdr_id,
    dmnd_dte,
    array_agg(modified_dept_nm ignore nulls) as dept_nms,
    array_agg(modified_dept_nbr ignore nulls) as dept_nbrs,
    array_agg(nbr_zid ignore nulls) as nbr_zids,
    array_agg(ffld_net_chrgd_amt) as sale_prices,
    array_agg(sku_nbr) as skus
from `kohls-bda-mkt-prd.dp_marketing_sandbox.bk_email_recomm_v0`
group by 1, 2, 3
order by dmnd_dte asc;


-- 
-- ********** Data Prep  ********** 
-- 

#standardSQL
create or replace table `kohls-bda-mkt-prd.dp_marketing_sandbox.bk_email_recomm_v2` as
with main as (
    SELECT m.*REPLACE(ARRAY(SELECT DISTINCT el FROM m.nbr_zids AS el) AS nbr_zids), 
    ROW_NUMBER() OVER (PARTITION BY cust_id ORDER BY dmnd_dte DESC) AS rn 
    FROM `kohls-bda-mkt-prd.dp_marketing_sandbox.bk_email_recomm_v1` m 
),     
max_deps as (
        select cust_id, 
            num_purchases, --total baskets
            ARRAY(SELECT DISTINCT x FROM UNNEST(concatenated) as x) as tot_cust_depts --List of purchased departments (cleaning the duplicate depts)
        from (
            select cust_id, ARRAY_CONCAT_AGG(nbr_zids) as concatenated, count(distinct mstr_trn_hdr_id) as num_purchases
            FROM main
            GROUP BY cust_id
        )
)

select 
    main.cust_id, 
    mstr_trn_hdr_id, 
    dmnd_dte,
    nbr_zids, 
    array_length(nbr_zids) as total_basket_deps,
    array_length(max_deps.tot_cust_depts) as tot_cust_deps,
    -- max_deps.tot_cust_depts as cust_deps,
    max_deps.num_purchases,
    main.rn as tran_rnk
    -- ,
    -- array(
    --     select all_dep
    --     from unnest(total_deps) all_dep 
    --     left join unnest(max_deps.tot_cust_depts) pos_dep
    --     on pos_dep = all_dep
    --     where pos_dep is null
    --     ) neg_deps
from main --, deps_mapp
left join max_deps on max_deps.cust_id = main.cust_id
order by dmnd_dte
;




-- Master Data which we use to create sequences in IPython Notebook
-- **********  Data Filteration, assigning rnd_val column to do sampling ********** 
-- 

-- https://cloud.google.com/bigquery/docs/reference/standard-sql/arrays#building_arrays_of_arrays

create or replace table `kohls-bda-mkt-prd.dp_marketing_sandbox.bk_email_recomm_v3` as
select cust_id, array_agg(struct(nbr_zids) order by dmnd_dte) as dept_nbrs, RAND() as rnd_val
from `kohls-bda-mkt-prd.dp_marketing_sandbox.bk_email_recomm_v2`
-- where tot_cust_deps > 3 and num_purchases > 1  -- 9871810 rows
where tot_cust_deps > 3 and num_purchases > 1 and tran_rnk < 21
group by 1;

-- 
-- **********  Creating a separate table for negative_samples(BPR Loss) ********** 
-- 
#standardSQL
create or replace table `kohls-bda-mkt-prd.dp_marketing_sandbox.bk_email_recomm_v4` as
with main as (
    SELECT m.*REPLACE(ARRAY(SELECT DISTINCT el FROM m.nbr_zids AS el) AS nbr_zids), 
    ROW_NUMBER() OVER (PARTITION BY cust_id ORDER BY dmnd_dte DESC) AS rn 
    FROM `kohls-bda-mkt-prd.dp_marketing_sandbox.bk_email_recomm_v1` m 
),     
max_deps as (
        select cust_id, num_purchases, ARRAY(SELECT DISTINCT x FROM UNNEST(concatenated) as x) as tot_cust_depts
        from (
            select cust_id, ARRAY_CONCAT_AGG(nbr_zids) as concatenated, count(distinct mstr_trn_hdr_id) as num_purchases
            FROM main
            where rn !=1 and rn < 21
            GROUP BY 1
        )
), deps_mapp as (
    select ARRAY(
    SELECT zid as nbr_zid
    FROM  `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_majcls_zid_mapping`
    ) as total_deps
)
select 
    max_deps.cust_id, 
    -- mstr_trn_hdr_id, 
    -- nbr_zids, 
    -- array_length(nbr_zids) as total_basket_deps,
    array_length(max_deps.tot_cust_depts) as tot_cust_deps,
    max_deps.tot_cust_depts as cust_deps,
    max_deps.num_purchases,
    array(
        select all_dep
        from unnest(deps_mapp.total_deps) all_dep 
        left join unnest(max_deps.tot_cust_depts) pos_dep
        on pos_dep = all_dep
        where pos_dep is null
        ) neg_deps
from max_deps, deps_mapp;
