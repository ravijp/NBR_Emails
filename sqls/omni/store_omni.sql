------------------INFORMATION-------------------
----------Data Build for Store-Omni Models----------
-----------Author: Ravi Prakash-----------------
-----------Last Modified: 10/26/2022------------
------------------------------------------------

DECLARE vantage_date date DEFAULT '2021-09-30';
DECLARE first_tran_dt date DEFAULT '2020-10-01';
DECLARE omni_start_dt date DEFAULT '2021-10-01';
DECLARE omni_year_dt date DEFAULT '2022-09-30';
DECLARE omni_halfstart_dt date DEFAULT '2022-04-01';
DECLARE omni_halfyear_dt date DEFAULT '2022-03-31';

-- OOT Sample
DECLARE oot_vantage_date date DEFAULT '2020-09-30';
DECLARE oot_first_tran_dt date DEFAULT '2019-10-01';
DECLARE oot_omni_start_dt date DEFAULT '2020-10-01';
DECLARE oot_omni_year_dt date DEFAULT '2021-09-30';
DECLARE oot_omni_halfstart_dt date DEFAULT '2021-04-01';
DECLARE oot_omni_halfyear_dt date DEFAULT '2021-03-31';

-- Population (cust_id level)
---- store only shoppers, excluding new customers (customers with < 1 year tenure, meaning the first transaction date is within 1 year by vantage date)

-- Time Range 
---- vantage date 9/30/2021, store only shoppers (from transactions 10/1/2020- 9/30/2021). 
---- Model 1: if they turned into omni in 10/1/2021 - 9/30/2022 or not; 
---- Model 2: if they stayed store-only 10/1/2021 - 3/31/2022 but turning omni 4/1/2022 - 9/30/2022 or not

SELECT 
    (CASE WHEN STORE_TRANS > 0 and ECOM_TRANS=0 then 1 else 0 end) as store_only, 
    count(*)
    FROM (
        SELECT
            CUST_ID,
            IFNULL(COUNT(DISTINCT(CASE WHEN ECOM_IND = 'Y' THEN GRO_MSTR_HDR_KY_TXT END)),0) AS ECOM_TRANS,
            IFNULL(COUNT(DISTINCT(CASE WHEN ECOM_IND = 'N' THEN GRO_MSTR_HDR_KY_TXT END)),0) AS STORE_TRANS,
        FROM
            `kohls-bda-prd.dp_marketing.bqt_sls_cust_agg`
        WHERE
            DMND_DTE BETWEEN first_tran_dt AND vantage_date
            AND CUST_ID IS NOT NULL
            AND SLS_TYP_CDE = 'S'
        GROUP BY 1
        )
    GROUP BY 1;


SELECT 
    (CASE WHEN STORE_FLAG > 0 and ECOM_FLAG=0 then 1 else 0 end) as store_only, 
    count(*), count(distinct cust_id)
    FROM (
        SELECT
            CUST_ID,
            SUM(CASE WHEN ECOM_IND = 'Y' THEN 1 ELSE 0 END) AS ECOM_FLAG,
            SUM(CASE WHEN ECOM_IND = 'N' THEN 1 ELSE 0 END) AS STORE_FLAG,
        FROM
            `kohls-bda-prd.dp_marketing.bqt_sls_cust_agg`
        WHERE
            DMND_DTE BETWEEN first_tran_dt AND vantage_date
            AND CUST_ID IS NOT NULL
            AND SLS_TYP_CDE = 'S'
        GROUP BY 1
        )
    GROUP BY 1;


select 
distinct a.cust_id,
from `kohls-bda-prd.dp_marketing.bqt_sls_cust_agg` a
where a.cust_id is not null
and a.DMND_DTE between first_tran_dt and vantage_date     
and a.ECOM_IND = 'N' --in store cust only
and a.sls_typ_cde = 'S'
and a.STR_NBR not in (1534, 1535, 1536, 1537, 1538, 1539, 1540, 1541, 1542, 1543, 1544, 1545) --excluding some stores because these are outlet locations



-- 
-- 
-- 
------------------INFORMATION-------------------
----------Data Build for Store-Omni Models----------
-----------Author: Ravi Prakash-----------------
-----------Last Modified: 10/26/2022------------
------------------------------------------------

DECLARE vantage_date date DEFAULT '2021-09-30';
DECLARE first_tran_dt date DEFAULT '2020-10-01';
DECLARE omni_start_dt date DEFAULT '2021-10-01';
DECLARE omni_year_dt date DEFAULT '2022-09-30';
DECLARE omni_halfstart_dt date DEFAULT '2022-04-01';
DECLARE omni_halfyear_dt date DEFAULT '2022-03-31';

-- OOT Sample
DECLARE oot_vantage_date date DEFAULT '2020-09-30';
DECLARE oot_first_tran_dt date DEFAULT '2019-10-01';
DECLARE oot_omni_start_dt date DEFAULT '2020-10-01';
DECLARE oot_omni_year_dt date DEFAULT '2021-09-30';
DECLARE oot_omni_halfstart_dt date DEFAULT '2021-04-01';
DECLARE oot_omni_halfyear_dt date DEFAULT '2021-03-31';

SELECT 
    (CASE WHEN STORE_FLAG > 0 and ECOM_FLAG=0 then 1 else 0 end) as store_only, 
    count(*), count(distinct cust_id)
    FROM (
        SELECT
            CUST_ID,
            SUM(CASE WHEN ECOM_IND = 'Y' THEN 1 ELSE 0 END) AS ECOM_FLAG,
            SUM(CASE WHEN ECOM_IND = 'N' THEN 1 ELSE 0 END) AS STORE_FLAG,
        FROM
            `kohls-bda-prd.dp_marketing.bqt_sls_cust_agg`
        WHERE
            DMND_DTE BETWEEN first_tran_dt AND vantage_date
            AND CUST_ID IS NOT NULL
            AND SLS_TYP_CDE = 'S'
            AND STR_NBR not in (1534, 1535, 1536, 1537, 1538, 1539, 1540, 1541, 1542, 1543, 1544, 1545)
        GROUP BY 1
        )
    GROUP BY 1;


select 
count(distinct a.cust_id),
from `kohls-bda-prd.dp_marketing.bqt_sls_cust_agg` a
where a.cust_id is not null
and a.DMND_DTE between first_tran_dt and vantage_date     
and a.ECOM_IND = 'N' --in store cust only
and a.sls_typ_cde = 'S'
and a.STR_NBR not in (1534, 1535, 1536, 1537, 1538, 1539, 1540, 1541, 1542, 1543, 1544, 1545) --excluding some stores because these are outlet locations
