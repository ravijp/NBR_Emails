-- -- Select the dates

-- declare vantage_date DATE DEFAULT '2021-09-30';
-- declare data_start_date date DEFAULT (date_sub(vantage_date, interval 1 year))+1;
-- declare data_end_date date DEFAULT vantage_date;
-- declare dep_start_date date DEFAULT data_end_date+1;
-- declare dep_mid_end_date date DEFAULT (date_add(vantage_date, interval 6 month))+1;
-- declare dep_end_date date DEFAULT (date_add(vantage_date, interval 1 year));

-- select data_start_date, data_end_date, vantage_date, dep_start_date, dep_mid_end_date, dep_end_date;
-- -- OVR 


-- -- In-store customers : Train
-- create or replace table `kohls-bda-mkt-prd.aa_sandbox.ovs_train_str_cust_list`
-- -- OPTIONS (expiration_timestamp = TIMESTAMP_ADD(CURRENT_TIMESTAMP(), INTERVAL 24 HOUR))
--   as
--   select distinct str.cust_id,str.llty_acct_nbr,q2.cust_tenure from
--     (
--       (
--         select
--         distinct  
--         q1.cust_id,
--         max(q1.llty_acct_nbr) as llty_acct_nbr
--         from 
--         (
--           select 
--             distinct a.cust_id,
--             b.acct_nbr as llty_acct_nbr

--           from `kohls-bda-prd.dp_marketing.bqt_sls_cust_agg` a
--           left join `kohls-bda-prd.dp_customer.bqth_cust_acct_pit` b
--             on a.cust_id = b.cust_id
--               and b.acct_typ_cde='LOY'
--               and vantage_date between b.eff_dte and b.exp_dte
--           where a.cust_id is not null
--             and a.DMND_DTE between data_start_date and data_end_date     
--             and a.ECOM_IND = 'N' --in store cust only
--             and a.sls_typ_cde = 'S'
--             and a.STR_NBR not in (1534, 1535, 1536, 1537, 1538, 1539, 1540, 1541, 1542, 1543, 1544, 1545) --excluding some stores because these are outlet locations
--         )q1
--         group by 1
--         order by 1
--       )str
--     left join 
--       (
--         select 
--           distinct cust_id,date_diff(vantage_date,min(dmnd_dte),day) as cust_tenure
--           from `kohls-bda-prd.dp_marketing.bqt_sls_cust_agg`
--           where sls_typ_cde = 'S'
--           group by 1 
--       ) q2
--     on str.cust_id = q2.cust_id
--     and q2.cust_tenure > 365
--     )
--   ;
--   --  58,606,246 records

-- -- Dep Variables : Train Data
-- create or replace table `kohls-bda-mkt-prd.aa_sandbox.ovs_train_cust_dep_vars`
-- -- OPTIONS (expiration_timestamp = TIMESTAMP_ADD(CURRENT_TIMESTAMP(), INTERVAL 24 HOUR))
--   as
--   select 
--   q1.*,
--   case when q1.ecom_sales_per1 + q1.ecom_sales_per2 > 0 then 1 else 0 end out_model1,
--   case when q1.ecom_sales_per1 = 0 and q1.ecom_sales_per2 > 0 then 1 else 0 end out_model2
--   from
--   (
--   select
--     distinct b.cust_id,

--     -- Store sales : period 1
--     case when min(a.DMND_DTE) between dep_start_date and dep_mid_end_date
--     and min(a.ECOM_IND) = 'N' then sum(a.dmnd_net_chrgd_amt) else 0 end 
--     as str_sales_per1,

--     --ecom sales : period 1
--     case when min(a.DMND_DTE) between dep_start_date and dep_mid_end_date
--     and min(a.ECOM_IND) = 'Y' then sum(a.dmnd_net_chrgd_amt) else 0 end 
--     as ecom_sales_per1,

--     -- Store sales : period 2
--     case when max(a.DMND_DTE) between dep_mid_end_date+1 and dep_end_date
--     and min(a.ECOM_IND) = 'N' then sum(a.dmnd_net_chrgd_amt) else 0 end 
--     as str_sales_per2,

--     --ecom sales : period 2
--     case when max(a.DMND_DTE) between dep_mid_end_date+1 and dep_end_date
--     and min(a.ECOM_IND) = 'Y' then sum(a.dmnd_net_chrgd_amt) else 0 end 
--     as ecom_sales_per2,

--     ifnull(sum(a.dmnd_net_chrgd_amt),0) as tota_12m_sls

--   from `kohls-bda-prd.dp_marketing.bqt_sls_cust_agg` a
--   right join `kohls-bda-mkt-prd.aa_sandbox.ovs_train_str_cust_list` b
--     on a.cust_id = b.cust_id
--     and a.DMND_DTE between dep_start_date and dep_end_date
--     and a.cust_id is not null
--     and a.sls_typ_cde = 'S'
--     and a.STR_NBR not in (1534, 1535, 1536, 1537, 1538, 1539, 1540, 1541, 1542, 1543, 1544, 1545) --excluding some stores because these are outlet locations
--   group by 1
--   ) q1;
-- --  58,606,246 records

-- select count(distinct cust_id),sum(out_model1),sum(out_model2) from
-- `kohls-bda-mkt-prd.aa_sandbox.ovs_train_cust_dep_vars`
-- where tota_12m_sls >0;


-- select distinct acct_opn_sub_chnl_id from `kohls-bda-prd.dp_customer.bqth_cust_acct_pit`;


-- -- Customer's campaign specific information at data end date
-- create or replace table `kohls-bda-mkt-prd.aa_sandbox.ovs_train_str_cust_demog_pit`
-- OPTIONS (expiration_timestamp = TIMESTAMP_ADD(CURRENT_TIMESTAMP(), INTERVAL 24 HOUR))
--   as
--   select
--     x.cust_id, 
--     case when p.email_ind = 'Y' then 1 when p.email_ind = 'N' then 0 else null end as email_ind,
--     case when p.malbl_ind = 'Y' then 1 when p.malbl_ind = 'N' then 0 else null end as malbl_ind,
--     -- case when p.cust_prefr_st_cde = 'Y' then 1 when p.cust_prefr_st_cde = 'N' then 0 else null end as cust_prefr_st_cde,
--     -- case when p.atg_rgstd_prfl_ind = 'Y' then 1 when p.atg_rgstd_prfl_ind = 'N' then 0 else null end as atg_rgstd_prfl_ind,
--     case when p.kohls_assoc_ind = 'Y' then 1 when p.kohls_assoc_ind = 'N' then 0 else null end as kohls_assoc_ind,
--     case when p.sms_optd_in_ind = 'Y' then 1 when p.sms_optd_in_ind = 'N' then 0 else null end as sms_optd_in_ind,
--     case when p.push_notif_optd_in_ind = 'Y' then 1 when p.push_notif_optd_in_ind = 'N' then 0 else null end as push_notif_optd_in_ind,
--     case when p.kc_crdhldr_ind = 'Y' then 1 when p.kc_crdhldr_ind = 'N' then 0 else null end as kc_crdhldr_ind,
--     case when p.kc_mvc_ind = 'Y' then 1 when p.kc_mvc_ind = 'N' then 0 else null end as kc_mvc_ind,
--     case when p.llty_acct_ind = 'Y' then 1 when p.llty_acct_ind = 'N' then 0 else null end as llty_acct_ind,
--     case when em.eml_sales_alert_opt_in = 'I' then 1 else 0 end as eml_sales_alert_opt_in,
    
--   from `kohls-bda-mkt-prd.aa_sandbox.ovs_train_str_cust_list` x
--   left join `kohls-bda-prd.dp_customer.bqth_cust_pit` p
--   on x.cust_id = p.cust_id and data_end_date between p.eff_dte and p.eff_end_dte
--   left join `kohls-bda-prd.dp_marketing.bqt_mktg_email_attributes` em
--   on x.cust_id = em.cust_id
--     and extract(date from em.eml_sales_alert_opt_in_dte) <= data_end_date
--   order by cust_id;

-- -- Customer's demographics information at data end date
-- create or replace table `kohls-bda-mkt-prd.aa_sandbox.ovs_train_str_cust_demog_cfv`
-- OPTIONS (expiration_timestamp = TIMESTAMP_ADD(CURRENT_TIMESTAMP(), INTERVAL 24 HOUR))
--   as
--   select
--     x.cust_id, 
--     case when p.fmale_ind = 'Y' then 1
--          when p.fmale_ind = 'N' then 0
--        else null end 
--     as fmale_ind, --gender

--     p.cust_age, --age

--     p.hh_size_cde, --household size

--     case when p.prsnc_chld_ind = 'Y' then 1
--          when p.prsnc_chld_ind = 'N' then 0
--     else null end 
--     as prsnc_chld_ind, --children's prescence

--     -- p.mrtl_stat_cde, --marital status
--     case when p.mrtl_stat_cde in  ('M','A') then 1
--          when p.mrtl_stat_cde in ('S', 'B') then 0
--     else null end 
--     as married_ind, --marital status   

--     -- p.nbr_chldn_cde as nbr_chldn_cde2, --number of children
--     case when p.nbr_chldn_cde = '' then null when p.nbr_chldn_cde is null then null else p.nbr_chldn_cde end as nbr_chldn_cde,


--     -- p.nw_mvr_ind, --new mover
--     case when p.nw_mvr_ind = 'Y' then 1 when p.nw_mvr_ind = '' then null when p.nw_mvr_ind is null then null else 0 end as nw_mvr_Y_ind,

--     -- p.race_lo_dtl_cde, --race

--     case when p.race_lo_dtl_cde = 'W' then 1 when p.race_lo_dtl_cde = '' then null when p.race_lo_dtl_cde is null then null else 0 end as race_W_ind,
--     case when p.race_lo_dtl_cde = 'H' then 1 when p.race_lo_dtl_cde = '' then null when p.race_lo_dtl_cde is null then null else 0 end as race_H_ind,
--     case when p.race_lo_dtl_cde = 'B' then 1 when p.race_lo_dtl_cde = '' then null when p.race_lo_dtl_cde is null then null else 0 end as race_B_ind,
--     case when p.race_lo_dtl_cde = 'A' then 1 when p.race_lo_dtl_cde = '' then null when p.race_lo_dtl_cde is null then null else 0 end as race_A_ind,

--     -- p.fam_lfstg, --family life style
--     case when p.fam_lfstg = 'Other Shoppers' then 1 when p.fam_lfstg is null then null else 0 end as fam_lfstg_Other_Shoppers_ind,
--     case when p.fam_lfstg = 'Girls/Boys' then 1 when p.fam_lfstg is null then null else 0 end as fam_lfstg_Girls_Boys_ind,
--     case when p.fam_lfstg = 'Multi-Age' then 1 when p.fam_lfstg is null then null else 0 end as fam_lfstg_Multi_Age_ind,
--     case when p.fam_lfstg = 'Older Teens' then 1 when p.fam_lfstg is null then null else 0 end as fam_lfstg_Older_Teens_ind,
--     case when p.fam_lfstg = 'Infant/Toddler' then 1 when p.fam_lfstg is null then null else 0 end as fam_lfstg_Infant_Toddler_ind,

--     -- p.hm_ownr_rntr_src1_cde, --home owner
--     case when p.hm_ownr_rntr_src1_cde = 'O' then 1 when p.hm_ownr_rntr_src1_cde = '' then null when p.hm_ownr_rntr_src1_cde is null then null else 0 end as hm_ownr_O_ind,
--     case when p.hm_ownr_rntr_src1_cde = 'R' then 1 when p.hm_ownr_rntr_src1_cde = '' then null when p.hm_ownr_rntr_src1_cde is null then null else 0 end as hm_ownr_R_ind,

--   from `kohls-bda-mkt-prd.aa_sandbox.ovs_train_str_cust_list` x
--   left join `kohls-bda-prd.dp_customer.bqth_cust_demog` p
--   on x.cust_id = p.cust_id and data_end_date between p.eff_dte and p.eff_end_dte
--   order by cust_id;

-- --kohl's card and kohl's reward tenure at a point in time
-- CREATE OR REPLACE TABLE `kohls-bda-mkt-prd.aa_sandbox.ovs_train_str_cust_tenures_acct_pit`
-- OPTIONS (expiration_timestamp = TIMESTAMP_ADD(CURRENT_TIMESTAMP(), INTERVAL 24 HOUR))
--   AS
--   select
--     x.cust_id,
--     max(case when acct_typ_cde = 'KC' then p.acct_opn_dte else null end) as kc_acct_opn_dte,
--     ifnull(DATE_DIFF(data_end_date, max(case when p.acct_typ_cde = 'KC' then p.acct_opn_dte else null end), DAY)+1,0) as kc_tenure_days,
--     max(case when p.acct_typ_cde = 'LOY' then p.acct_opn_dte else null end) as kr_acct_opn_dte,
--     ifnull(DATE_DIFF(data_end_date, max(case when p.acct_typ_cde = 'LOY' then p.acct_opn_dte else null end), DAY)+1,0) as kr_tenure_days,
  
--   from `kohls-bda-mkt-prd.aa_sandbox.ovs_train_str_cust_list` x
--   left join `kohls-bda-prd.dp_customer.bqth_cust_acct_pit` p
--   on x.cust_id = p.cust_id and data_end_date between p.eff_dte and p.exp_dte
--   group by 1;


-- Select the dates

declare vantage_date DATE DEFAULT '2021-09-30';
declare data_start_date date DEFAULT (date_sub(vantage_date, interval 1 year))+1;
declare data_end_date date DEFAULT vantage_date;
declare dep_start_date date DEFAULT data_end_date+1;
declare dep_mid_end_date date DEFAULT (date_add(vantage_date, interval 6 month))+1;
declare dep_end_date date DEFAULT (date_add(vantage_date, interval 1 year));

select data_start_date, data_end_date, vantage_date, dep_start_date, dep_mid_end_date, dep_end_date;

create or replace table `kohls-bda-mkt-prd.aa_sandbox.ovs_train_str_cust_list_test`
OPTIONS (expiration_timestamp = TIMESTAMP_ADD(CURRENT_TIMESTAMP(), INTERVAL 24 HOUR))
as
select 
  distinct a.cust_id,
  max(b.acct_nbr) as llty_acct_nbr,
  count(distinct case when a.ECOM_IND = 'N' then a.mstr_trn_hdr_id else null end) as store_trans,
  count(distinct case when a.ECOM_IND = 'Y' then a.mstr_trn_hdr_id else null end) as ecom_trans,
  count(distinct a.mstr_trn_hdr_id) as tot_trans

from `kohls-bda-prd.dp_marketing.bqt_sls_cust_agg` a
left join `kohls-bda-prd.dp_customer.bqth_cust_acct_pit` b
  on a.cust_id = b.cust_id
    and b.acct_typ_cde='LOY'
    and vantage_date between b.eff_dte and b.exp_dte
where a.cust_id is not null
  and a.DMND_DTE between data_start_date and data_end_date     
  -- and a.ECOM_IND = 'N' --in store cust only
  and a.sls_typ_cde = 'S'
  and a.STR_NBR not in (1534, 1535, 1536, 1537, 1538, 1539, 1540, 1541, 1542, 1543, 1544, 1545) --excluding some stores because these are outlet locations
group by 1;

-- OVR 
-- Select the dates

declare vantage_date DATE DEFAULT '2021-09-30';
declare data_start_date date DEFAULT (date_sub(vantage_date, interval 1 year))+1;
declare data_end_date date DEFAULT vantage_date;
declare dep_start_date date DEFAULT data_end_date+1;
declare dep_mid_end_date date DEFAULT (date_add(vantage_date, interval 6 month))+1;
declare dep_end_date date DEFAULT (date_add(vantage_date, interval 1 year));

select data_start_date, data_end_date, vantage_date, dep_start_date, dep_mid_end_date, dep_end_date;

-- In-store customers : Train
create or replace table `kohls-bda-mkt-prd.aa_sandbox.ovs_train_str_cust_list`
-- OPTIONS (expiration_timestamp = TIMESTAMP_ADD(CURRENT_TIMESTAMP(), INTERVAL 24 HOUR))
  as
  select distinct str.cust_id,str.llty_acct_nbr,q2.cust_tenure from
  (
    (
      select 
        distinct a.cust_id,
        max(b.acct_nbr) as llty_acct_nbr,
        count(distinct case when a.ECOM_IND = 'N' then a.mstr_trn_hdr_id else null end) as store_trans,
        count(distinct case when a.ECOM_IND = 'Y' then a.mstr_trn_hdr_id else null end) as ecom_trans,
        count(distinct a.mstr_trn_hdr_id) as tot_trans

      from `kohls-bda-prd.dp_marketing.bqt_sls_cust_agg` a
      left join `kohls-bda-prd.dp_customer.bqth_cust_acct_pit` b
        on a.cust_id = b.cust_id
          and b.acct_typ_cde='LOY'
          and vantage_date between b.eff_dte and b.exp_dte
      where a.cust_id is not null
        and a.DMND_DTE between data_start_date and data_end_date
        and a.sls_typ_cde = 'S'
        and a.STR_NBR not in (1534, 1535, 1536, 1537, 1538, 1539, 1540, 1541, 1542, 1543, 1544, 1545) --excluding some stores because these are outlet locations
      group by 1
      having ecom_trans = 0
      )str

    left join 
      (
        select 
          distinct cust_id,date_diff(vantage_date,min(dmnd_dte),day) as cust_tenure
          from `kohls-bda-prd.dp_marketing.bqt_sls_cust_agg`
          where sls_typ_cde = 'S'
          group by 1 
      ) q2
    on str.cust_id = q2.cust_id
    and q2.cust_tenure > 365
  );
  --  58,606,246 records

-- Select the dates

declare vantage_date DATE DEFAULT '2021-09-30';
declare data_start_date date DEFAULT (date_sub(vantage_date, interval 1 year))+1;
declare data_end_date date DEFAULT vantage_date;
declare dep_start_date date DEFAULT data_end_date+1;
declare dep_mid_end_date date DEFAULT (date_add(vantage_date, interval 6 month))+1;
declare dep_end_date date DEFAULT (date_add(vantage_date, interval 1 year));

select data_start_date, data_end_date, vantage_date, dep_start_date, dep_mid_end_date, dep_end_date;

-- Customer's KC earns previous 12 months
create or replace table `kohls-bda-mkt-prd.aa_sandbox.ovs_train_str_cust_kc_earns`
OPTIONS (expiration_timestamp = TIMESTAMP_ADD(CURRENT_TIMESTAMP(), INTERVAL 24 HOUR))
  as
  select distinct
    x.cust_id,
    p.cpn_nbr,
    sum(p.rwd_trn_actvy_amt) as kc_earned,
  from `kohls-bda-mkt-prd.aa_sandbox.ovs_train_str_cust_list` x
  left join `kohls-bda-prd.dp_marketing.bqt_cust_issud_krwds` p
    on x.cust_id = p.cust_id
      and p.rwd_actvy_desc = 'Issue'
      and p.rwd_void_ind='N'
      and p.rwd_dte between data_start_date and data_end_date
      and p.cust_id is not null
      and p.kcsh_type in ('KCSH')
  right join `kohls-bda-prd.dp_marketing.bqv_mktg_ofr` o
    on cast(p.ofr_id as string) = o.offer_id
    and o.is_test_offer='Not a test'
    and lower(o.main_type) in ('kohls_cash')
  group by 1,2
  order by 1,2;

-- Customer's KR earns previous 12 months
create or replace table `kohls-bda-mkt-prd.aa_sandbox.ovs_train_str_cust_kr_earns`
OPTIONS (expiration_timestamp = TIMESTAMP_ADD(CURRENT_TIMESTAMP(), INTERVAL 24 HOUR))
  as
  select distinct
    x.cust_id,
    p.brcde_nbr,
    sum(cast(p.ofr_disc_amt as int)) as kr_earned,
  from `kohls-bda-mkt-prd.aa_sandbox.ovs_train_str_cust_list` x
  left join `kohls-bda-mkt-prd.cl_active.clt_loyalty_hist` p
    on cast(x.llty_acct_nbr as string) = cast(p.llty_acct_nbr as string)
  right join `kohls-bda-prd.dp_marketing.bqv_mktg_ofr` o
    on cast(p.ofr_id as string) = o.offer_id
    and o.is_test_offer='Not a test'
    and o.offer_type in ('Kohls_Rewards', 'Y2Y_Rewards')
    and lower(o.main_type) in ('kohls_cash', 'y2y_rewards')
    and o.offer_system='RMA'
  where 
    p.llty_earn_dte between data_start_date and data_end_date
  group by 1,2
  order by 1,2;

-- Customer's KC redeems for coupons issued in previous 12 months
create or replace table `kohls-bda-mkt-prd.aa_sandbox.ovs_train_str_cust_kc_redeems`
OPTIONS (expiration_timestamp = TIMESTAMP_ADD(CURRENT_TIMESTAMP(), INTERVAL 24 HOUR))
  as
  select distinct
    x.cust_id,
    p.cpn_nbr,
    sum(p.tot_ofr_disc_amt) as kc_redeemed,
  from `kohls-bda-mkt-prd.aa_sandbox.ovs_train_str_cust_list` x
  left join `kohls-bda-prd.dp_marketing.bqt_cust_redm_krwds` p
    on x.cust_id = p.cust_id
  left join `kohls-bda-mkt-prd.aa_sandbox.ovs_train_str_cust_kc_earns` e
    on p.cust_id = e.cust_id and p.cpn_nbr = e.cpn_nbr
  where 
    p.rwd_actvy_desc = 'Redeem'
    and p.dmnd_dte between data_start_date and data_end_date
    and p.cust_id is not null
    and p.cpn_nbr is not null
  group by 1,2
  order by 1,2;

-- Customer's KR redeems for coupons issued in previous 12 months
create or replace table `kohls-bda-mkt-prd.aa_sandbox.ovs_train_str_cust_kr_redeems`
OPTIONS (expiration_timestamp = TIMESTAMP_ADD(CURRENT_TIMESTAMP(), INTERVAL 24 HOUR))
  as
  select distinct
    x.cust_id,
    p.cpn_nbr,
    sum(p.tot_ofr_disc_amt) as kr_redeemed,
  from `kohls-bda-mkt-prd.aa_sandbox.ovs_train_str_cust_list` x
  left join `kohls-bda-prd.dp_marketing.bqt_cust_redm_krwds` p
    on x.cust_id = p.cust_id
  left join `kohls-bda-mkt-prd.aa_sandbox.ovs_train_str_cust_kr_earns` e
    on p.cust_id = e.cust_id and p.cpn_nbr = e.brcde_nbr
  where 
    p.rwd_actvy_desc = 'Redeem'
    and p.cpn_nbr is not null
    and p.dmnd_dte between data_start_date and data_end_date
    and p.cust_id is not null
  group by 1,2
  order by 1,2;

-- Join the KC redeems, earns to get the redemption rate
create or replace table `kohls-bda-mkt-prd.aa_sandbox.ovs_train_str_cust_kc_earns_redeems`
OPTIONS (expiration_timestamp = TIMESTAMP_ADD(CURRENT_TIMESTAMP(), INTERVAL 24 HOUR))
  as
  select e.cust_id,
    sum(e.kc_earned) as kc_earned_dollars,
    count(distinct e.cpn_nbr) as kc_earned_coupons,
    ifnull(sum(r.kc_redeemed),0) as kc_redeemed_dollars,
    ifnull(count(distinct r.cpn_nbr),0) as kc_redeemed_coupons,
  from  `kohls-bda-mkt-prd.aa_sandbox.ovs_train_str_cust_kc_earns` e
  left join `kohls-bda-mkt-prd.aa_sandbox.ovs_train_str_cust_kc_redeems` r
  on e.cust_id = r.cust_id and e.cpn_nbr = r.cpn_nbr
  group by 1
  order by 1;

-- Join the KR redeems, earns to get the redemption rate
create or replace table `kohls-bda-mkt-prd.aa_sandbox.ovs_train_str_cust_kr_earns_redeems`
OPTIONS (expiration_timestamp = TIMESTAMP_ADD(CURRENT_TIMESTAMP(), INTERVAL 24 HOUR))
  as
  select e.cust_id,
    sum(e.kr_earned) as kr_earned_dollars,
    count(distinct e.brcde_nbr) as kr_earned_coupons,
    ifnull(sum(r.kr_redeemed),0) as kr_redeemed_dollars,
    ifnull(count(distinct r.cpn_nbr),0) as kr_redeemed_coupons,
  from  `kohls-bda-mkt-prd.aa_sandbox.ovs_train_str_cust_kr_earns` e
  left join `kohls-bda-mkt-prd.aa_sandbox.ovs_train_str_cust_kr_redeems` r
  on e.cust_id = r.cust_id and e.brcde_nbr = r.cpn_nbr
  group by 1
  order by 1;


/************************************ J - Dec 22 PYD Campaign Data ************************************/

-- Get the loyalty account numbers of customers
create or replace table `kohls-bda-mkt-prd.aa_sandbox.pst_uplift_dec22_pyd_cust_list`
as
  select 
  q1.cust_id,
  max(q1.KC_IND) as kc_ind_src,
  max(q1.llty_acct_nbr) as llty_acct_nbr
  from 
    (
    select 
    distinct a.* ,
    (b.acct_nbr) as llty_acct_nbr
    
    from `kohls-bda-mkt-prd.ts_active.eml_20221201_pyd_to_aa_model_custs` a
    left join `kohls-bda-prd.dp_customer.bqth_cust_acct_pit` b
      on a.cust_id = b.cust_id
      and a.cust_id is not null
      and b.acct_typ_cde='LOY'
      and cast('2022-10-26' as date) between b.eff_dte and b.exp_dte
    )q1
  group by q1.cust_id
  order by q1.cust_id;
--   -- 1,927,998 rows


-- Select the dates
DECLARE cpgn_start_date,cpgn_end_date,vantage_date,data_end_date,data_start_date DATE;
SET (cpgn_start_date,cpgn_end_date,vantage_date,data_end_date,data_start_date) = (select AS STRUCT cpgn_start_date,cpgn_end_date,vantage_date,data_end_date,data_start_date
 from `kohls-bda-mkt-prd.aa_sandbox.pst_uplift_cpgn_dates_uplift`
 where cpgn = 'Dec-22 $10 PYD Gift');
select cpgn_start_date,cpgn_end_date,vantage_date,data_start_date,data_end_date;

-- Customer level features sales,transactions for 12 months prior from vantage point
create or replace table `kohls-bda-mkt-prd.aa_sandbox.pst_uplift_dec22_pyd_cust_tst_12m`
-- OPTIONS (expiration_timestamp = TIMESTAMP_ADD(CURRENT_TIMESTAMP(), INTERVAL 24 HOUR))
  as
  select 
    distinct b.cust_id,
    max(a.kc_acct_opn_dte) as kc_opn_dte,

    -- Transactions
    ifnull(COUNT(DISTINCT a.mstr_trn_hdr_id),0) AS num_tot_trans, --total transactions

      -- private vs national
      ifnull(COUNT(DISTINCT(case when TRIM(a.brnd_typ_cde) in ("P","E")   THEN a.mstr_trn_hdr_id end)),0) as num_privb_trans,
      ifnull(COUNT(DISTINCT(case when TRIM(a.brnd_typ_cde) = "R"  THEN a.mstr_trn_hdr_id end)),0) as num_natb_trans,
      ifnull(COUNT(DISTINCT(case when TRIM(a.brnd_typ_cde) = "NA"  THEN a.mstr_trn_hdr_id end)),0) as num_nullb_trans,

      -- Regular vs Clearance
      ifnull( COUNT(DISTINCT(case when TRIM(a.clr_sal_ind) = "N"  THEN a.mstr_trn_hdr_id end)),0) as num_reg_trans,
      ifnull( COUNT(DISTINCT(case when TRIM(a.clr_sal_ind) = "Y"  THEN a.mstr_trn_hdr_id end)),0) as num_clr_trans,
      ifnull( COUNT(DISTINCT(case when TRIM(a.clr_sal_ind) = "NA"  THEN a.mstr_trn_hdr_id end)),0) as num_nullc_trans,
      
      -- Timeline breakdown
      ifnull( count(distinct (case when date_diff(data_end_date,a.DMND_DTE,day) <= 30  THEN a.mstr_trn_hdr_id end)),0) as num_tot_trans_prev_1mnt, -- prev 1 month transactions
      ifnull( count(distinct (case when date_diff(data_end_date,a.DMND_DTE,day) <= 60  THEN a.mstr_trn_hdr_id end)),0) as num_tot_trans_prev_2mnt, -- prev 3 month transactions
      ifnull( count(distinct (case when date_diff(data_end_date,a.DMND_DTE,day) <= 90  THEN a.mstr_trn_hdr_id end)),0) as num_tot_trans_prev_3mnt, -- prev 6 month transactions
      ifnull( count(distinct (case when date_diff(data_end_date,a.DMND_DTE,day) <= 180  THEN a.mstr_trn_hdr_id end)),0) as num_tot_trans_prev_6mnt, -- prev 9 month transactions

      -- Transaction type
      ifnull(COUNT(DISTINCT(CASE WHEN kc_ind = 'Y' THEN a.mstr_trn_hdr_id END)),0) AS num_kc_trans, --kohls card transactions

      -- DMA Breakdown
      ifnull(count(distinct (case when a.dma_nbr = 1  THEN a.mstr_trn_hdr_id end )),0) as num_dma_home_trans,
      ifnull(count(distinct (case when a.dma_nbr = 23  THEN a.mstr_trn_hdr_id end )),0) as num_dma_mens_trans,
      ifnull(count(distinct (case when a.dma_nbr = 26  THEN a.mstr_trn_hdr_id end )),0) as num_dma_child_trans,
      ifnull(count(distinct (case when a.dma_nbr = 32  THEN a.mstr_trn_hdr_id end )),0) as num_dma_beauty_trans,
      ifnull(count(distinct (case when a.dma_nbr = 44  THEN a.mstr_trn_hdr_id end )),0) as num_dma_acc_jew_foot_trans,
      ifnull(count(distinct (case when a.dma_nbr = 45  THEN a.mstr_trn_hdr_id end )),0) as num_dma_disc_impul_trans,
      ifnull(count(distinct (case when a.dma_nbr = 50  THEN a.mstr_trn_hdr_id end )),0) as num_dma_seph_trans,
      ifnull(count(distinct (case when a.dma_nbr = 62  THEN a.mstr_trn_hdr_id end )),0) as num_dma_womens_trans,
      ifnull(count(distinct (case when a.dma_nbr = 63  THEN a.mstr_trn_hdr_id end )),0) as num_dma_ywomens_trans,
      ifnull(count(distinct (case when a.dma_nbr = 70  THEN a.mstr_trn_hdr_id end )),0) as num_dma_outlet_trans,
      ifnull(count(distinct (case when a.dma_nbr = 88  THEN a.mstr_trn_hdr_id end )),0) as num_dma_active_trans,
      ifnull(count(distinct (case when a.dma_nbr = 99  THEN a.mstr_trn_hdr_id end )),0) as num_dma_misc_trans,

      -- DMA * Timeline Breakdown
        -- Home
        ifnull(count(distinct (case when a.dma_nbr = 1 and date_diff(data_end_date,a.DMND_DTE,day) <=30  THEN a.mstr_trn_hdr_id end )),0) as num_dma_home_trans_prev_1mnt,
        ifnull(count(distinct (case when a.dma_nbr = 1 and date_diff(data_end_date,a.DMND_DTE,day) <=60  THEN a.mstr_trn_hdr_id end )),0) as num_dma_home_trans_prev_2mnt,
        ifnull(count(distinct (case when a.dma_nbr = 1 and date_diff(data_end_date,a.DMND_DTE,day) <=90  THEN a.mstr_trn_hdr_id end )),0) as num_dma_home_trans_prev_3mnt,
        ifnull(count(distinct (case when a.dma_nbr = 1 and date_diff(data_end_date,a.DMND_DTE,day) <=180  THEN a.mstr_trn_hdr_id end )),0) as num_dma_home_trans_prev_6mnt,

        -- Men's
        ifnull(count(distinct (case when a.dma_nbr = 23 and date_diff(data_end_date,a.DMND_DTE,day) <=30  THEN a.mstr_trn_hdr_id end )),0) as num_dma_mens_trans_prev_1mnt,
        ifnull(count(distinct (case when a.dma_nbr = 23 and date_diff(data_end_date,a.DMND_DTE,day) <=60  THEN a.mstr_trn_hdr_id end )),0) as num_dma_mens_trans_prev_2mnt,
        ifnull(count(distinct (case when a.dma_nbr = 23 and date_diff(data_end_date,a.DMND_DTE,day) <=90  THEN a.mstr_trn_hdr_id end )),0) as num_dma_mens_trans_prev_3mnt,
        ifnull(count(distinct (case when a.dma_nbr = 23 and date_diff(data_end_date,a.DMND_DTE,day) <=180  THEN a.mstr_trn_hdr_id end )),0) as num_dma_mens_trans_prev_6mnt,

        --Children's
        ifnull(count(distinct (case when a.dma_nbr = 26 and date_diff(data_end_date,a.DMND_DTE,day) <=30  THEN a.mstr_trn_hdr_id end )),0) as num_dma_child_trans_prev_1mnt,
        ifnull(count(distinct (case when a.dma_nbr = 26 and date_diff(data_end_date,a.DMND_DTE,day) <=60  THEN a.mstr_trn_hdr_id end )),0) as num_dma_child_trans_prev_2mnt,
        ifnull(count(distinct (case when a.dma_nbr = 26 and date_diff(data_end_date,a.DMND_DTE,day) <=90  THEN a.mstr_trn_hdr_id end )),0) as num_dma_child_trans_prev_3mnt,
        ifnull(count(distinct (case when a.dma_nbr = 26 and date_diff(data_end_date,a.DMND_DTE,day) <=180  THEN a.mstr_trn_hdr_id end )),0) as num_dma_child_trans_prev_6mnt,

        -- Beauty
        ifnull(count(distinct (case when a.dma_nbr = 32 and date_diff(data_end_date,a.DMND_DTE,day) <=30  THEN a.mstr_trn_hdr_id end )),0) as num_dma_beauty_trans_prev_1mnt,
        ifnull(count(distinct (case when a.dma_nbr = 32 and date_diff(data_end_date,a.DMND_DTE,day) <=60  THEN a.mstr_trn_hdr_id end )),0) as num_dma_beauty_trans_prev_2mnt,
        ifnull(count(distinct (case when a.dma_nbr = 32 and date_diff(data_end_date,a.DMND_DTE,day) <=90  THEN a.mstr_trn_hdr_id end )),0) as num_dma_beauty_trans_prev_3mnt,
        ifnull(count(distinct (case when a.dma_nbr = 32 and date_diff(data_end_date,a.DMND_DTE,day) <=180  THEN a.mstr_trn_hdr_id end )),0) as num_dma_beauty_trans_prev_6mnt,

        -- Acc/Jewel/Footwear
        ifnull(count(distinct (case when a.dma_nbr = 44 and date_diff(data_end_date,a.DMND_DTE,day) <=30  THEN a.mstr_trn_hdr_id end )),0) as num_dma_acc_jew_foot_trans_prev_1mnt,
        ifnull(count(distinct (case when a.dma_nbr = 44 and date_diff(data_end_date,a.DMND_DTE,day) <=60  THEN a.mstr_trn_hdr_id end )),0) as num_dma_acc_jew_foot_trans_prev_2mnt,
        ifnull(count(distinct (case when a.dma_nbr = 44 and date_diff(data_end_date,a.DMND_DTE,day) <=90  THEN a.mstr_trn_hdr_id end )),0) as num_dma_acc_jew_foot_trans_prev_3mnt,
        ifnull(count(distinct (case when a.dma_nbr = 44 and date_diff(data_end_date,a.DMND_DTE,day) <=180  THEN a.mstr_trn_hdr_id end )),0) as num_dma_acc_jew_foot_trans_prev_6mnt,

        -- Disc/Impluse
        ifnull(count(distinct (case when a.dma_nbr = 45 and date_diff(data_end_date,a.DMND_DTE,day) <=30  THEN a.mstr_trn_hdr_id end )),0) as num_dma_disc_impul_trans_prev_1mnt,
        ifnull(count(distinct (case when a.dma_nbr = 45 and date_diff(data_end_date,a.DMND_DTE,day) <=60  THEN a.mstr_trn_hdr_id end )),0) as num_dma_disc_impul_trans_prev_2mnt,
        ifnull(count(distinct (case when a.dma_nbr = 45 and date_diff(data_end_date,a.DMND_DTE,day) <=90  THEN a.mstr_trn_hdr_id end )),0) as num_dma_disc_impul_trans_prev_3mnt,
        ifnull(count(distinct (case when a.dma_nbr = 45 and date_diff(data_end_date,a.DMND_DTE,day) <=180  THEN a.mstr_trn_hdr_id end )),0) as num_dma_disc_impul_trans_prev_6mnt,

        -- Women's
        ifnull(count(distinct (case when a.dma_nbr = 62 and date_diff(data_end_date,a.DMND_DTE,day) <=30  THEN a.mstr_trn_hdr_id end )),0) as num_dma_womens_trans_prev_1mnt,
        ifnull(count(distinct (case when a.dma_nbr = 62 and date_diff(data_end_date,a.DMND_DTE,day) <=60  THEN a.mstr_trn_hdr_id end )),0) as num_dma_womens_trans_prev_2mnt,
        ifnull(count(distinct (case when a.dma_nbr = 62 and date_diff(data_end_date,a.DMND_DTE,day) <=90  THEN a.mstr_trn_hdr_id end )),0) as num_dma_womens_trans_prev_3mnt,
        ifnull(count(distinct (case when a.dma_nbr = 62 and date_diff(data_end_date,a.DMND_DTE,day) <=180  THEN a.mstr_trn_hdr_id end )),0) as num_dma_womens_trans_prev_6mnt,

        -- Young women's
        ifnull(count(distinct (case when a.dma_nbr = 63 and date_diff(data_end_date,a.DMND_DTE,day) <=30  THEN a.mstr_trn_hdr_id end )),0) as num_dma_ywomens_trans_prev_1mnt,
        ifnull(count(distinct (case when a.dma_nbr = 63 and date_diff(data_end_date,a.DMND_DTE,day) <=60  THEN a.mstr_trn_hdr_id end )),0) as num_dma_ywomens_trans_prev_2mnt,
        ifnull(count(distinct (case when a.dma_nbr = 63 and date_diff(data_end_date,a.DMND_DTE,day) <=90  THEN a.mstr_trn_hdr_id end )),0) as num_dma_ywomens_trans_prev_3mnt,
        ifnull(count(distinct (case when a.dma_nbr = 63 and date_diff(data_end_date,a.DMND_DTE,day) <=180  THEN a.mstr_trn_hdr_id end )),0) as num_dma_ywomens_trans_prev_6mnt,

        -- Outlet
        ifnull(count(distinct (case when a.dma_nbr = 70 and date_diff(data_end_date,a.DMND_DTE,day) <=30  THEN a.mstr_trn_hdr_id end )),0) as num_dma_outlet_trans_prev_1mnt,
        ifnull(count(distinct (case when a.dma_nbr = 70 and date_diff(data_end_date,a.DMND_DTE,day) <=60  THEN a.mstr_trn_hdr_id end )),0) as num_dma_outlet_trans_prev_2mnt,
        ifnull(count(distinct (case when a.dma_nbr = 70 and date_diff(data_end_date,a.DMND_DTE,day) <=90  THEN a.mstr_trn_hdr_id end )),0) as num_dma_outlet_trans_prev_3mnt,
        ifnull(count(distinct (case when a.dma_nbr = 70 and date_diff(data_end_date,a.DMND_DTE,day) <=180  THEN a.mstr_trn_hdr_id end )),0) as num_dma_outlet_trans_prev_6mnt,

        -- Active
        ifnull(count(distinct (case when a.dma_nbr = 88 and date_diff(data_end_date,a.DMND_DTE,day) <=30  THEN a.mstr_trn_hdr_id end )),0) as num_dma_active_trans_prev_1mnt,
        ifnull(count(distinct (case when a.dma_nbr = 88 and date_diff(data_end_date,a.DMND_DTE,day) <=60  THEN a.mstr_trn_hdr_id end )),0) as num_dma_active_trans_prev_2mnt,
        ifnull(count(distinct (case when a.dma_nbr = 88 and date_diff(data_end_date,a.DMND_DTE,day) <=90  THEN a.mstr_trn_hdr_id end )),0) as num_dma_active_trans_prev_3mnt,
        ifnull(count(distinct (case when a.dma_nbr = 88 and date_diff(data_end_date,a.DMND_DTE,day) <=180  THEN a.mstr_trn_hdr_id end )),0) as num_dma_active_trans_prev_6mnt,

        -- Misc
        ifnull(count(distinct (case when a.dma_nbr = 99 and date_diff(data_end_date,a.DMND_DTE,day) <=30  THEN a.mstr_trn_hdr_id end )),0) as num_dma_misc_trans_prev_1mnt,
        ifnull(count(distinct (case when a.dma_nbr = 99 and date_diff(data_end_date,a.DMND_DTE,day) <=60  THEN a.mstr_trn_hdr_id end )),0) as num_dma_misc_trans_prev_2mnt,
        ifnull(count(distinct (case when a.dma_nbr = 99 and date_diff(data_end_date,a.DMND_DTE,day) <=90  THEN a.mstr_trn_hdr_id end )),0) as num_dma_misc_trans_prev_3mnt,
        ifnull(count(distinct (case when a.dma_nbr = 99 and date_diff(data_end_date,a.DMND_DTE,day) <=180  THEN a.mstr_trn_hdr_id end )),0) as num_dma_misc_trans_prev_6mnt,

    
    -- Sales
    ifnull(SUM(a.DMND_NET_CHRGD_AMT),0) AS SALES, --total sales

      -- private vs national
      ifnull(sum(case when TRIM(a.brnd_typ_cde) in ("P","E")  then a.DMND_NET_CHRGD_AMT else 0 end),0) as tot_privb_sales,
      ifnull(sum(case when TRIM(a.brnd_typ_cde) = "R" then a.DMND_NET_CHRGD_AMT else 0 end),0) as tot_natb_sales,
      ifnull(sum(case when TRIM(a.brnd_typ_cde) = "NA" then a.DMND_NET_CHRGD_AMT else 0 end),0) as tot_nullb_sales,

      -- Regular vs Clearance
      ifnull(sum(case when TRIM(a.clr_sal_ind) = "N" then a.DMND_NET_CHRGD_AMT else 0 end),0) as tot_reg_sales,
      ifnull(sum(case when TRIM(a.clr_sal_ind) = "Y" then a.DMND_NET_CHRGD_AMT else 0 end),0) as tot_clr_sales,
      ifnull(sum(case when TRIM(a.clr_sal_ind) = "NA" then a.DMND_NET_CHRGD_AMT else 0 end),0) as tot_nullc_sales,
      
      -- Timeline Breakdown
      ifnull(sum((case when date_diff(data_end_date,a.DMND_DTE,day) <= 30 then a.DMND_NET_CHRGD_AMT end)),0) as tot_sls_prev_1mnt, -- prev 1 month sales
      ifnull(sum((case when date_diff(data_end_date,a.DMND_DTE,day) <= 60 then a.DMND_NET_CHRGD_AMT end)),0) as tot_sls_prev_2mnt, -- prev 3 month sales
      ifnull(sum((case when date_diff(data_end_date,a.DMND_DTE,day) <= 90 then a.DMND_NET_CHRGD_AMT end)),0) as tot_sls_prev_3mnt, -- prev 6 month sales
      ifnull(sum((case when date_diff(data_end_date,a.DMND_DTE,day) <= 180 then a.DMND_NET_CHRGD_AMT end)),0) as tot_sls_prev_6mnt, -- prev 9 month sales
     
      -- DMA Breakdown
      ifnull(sum((case when a.dma_nbr = 1 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_home_sls,
      ifnull(sum((case when a.dma_nbr = 23 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_mens_sls,
      ifnull(sum((case when a.dma_nbr = 26 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_child_sls,
      ifnull(sum((case when a.dma_nbr = 32 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_beauty_sls,
      ifnull(sum((case when a.dma_nbr = 44 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_acc_jew_foot_sls,
      ifnull(sum((case when a.dma_nbr = 45 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_disc_impul_sls,
      ifnull(sum((case when a.dma_nbr = 50 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_seph_sls,
      ifnull(sum((case when a.dma_nbr = 62 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_womens_sls,
      ifnull(sum((case when a.dma_nbr = 63 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_ywomens_sls,
      ifnull(sum((case when a.dma_nbr = 70 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_outlet_sls,
      ifnull(sum((case when a.dma_nbr = 88 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_active_sls,
      ifnull(sum((case when a.dma_nbr = 99 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_misc_sls,

      -- DMA * Timeline Breakdown
        -- Home
        ifnull(sum((case when a.dma_nbr = 1 and date_diff(data_end_date,a.DMND_DTE,day) <=30 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_home_sls_prev_1mnt,
        ifnull(sum((case when a.dma_nbr = 1 and date_diff(data_end_date,a.DMND_DTE,day) <=60 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_home_sls_prev_2mnt,
        ifnull(sum((case when a.dma_nbr = 1 and date_diff(data_end_date,a.DMND_DTE,day) <=90 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_home_sls_prev_3mnt,
        ifnull(sum((case when a.dma_nbr = 1 and date_diff(data_end_date,a.DMND_DTE,day) <=180 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_home_sls_prev_6mnt,

        -- Men's
        ifnull(sum((case when a.dma_nbr = 23 and date_diff(data_end_date,a.DMND_DTE,day) <=30 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_mens_sls_prev_1mnt,
        ifnull(sum((case when a.dma_nbr = 23 and date_diff(data_end_date,a.DMND_DTE,day) <=60 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_mens_sls_prev_2mnt,
        ifnull(sum((case when a.dma_nbr = 23 and date_diff(data_end_date,a.DMND_DTE,day) <=90 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_mens_sls_prev_3mnt,
        ifnull(sum((case when a.dma_nbr = 23 and date_diff(data_end_date,a.DMND_DTE,day) <=180 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_mens_sls_prev_6mnt,

        --Children's
        ifnull(sum((case when a.dma_nbr = 26 and date_diff(data_end_date,a.DMND_DTE,day) <=30 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_child_sls_prev_1mnt,
        ifnull(sum((case when a.dma_nbr = 26 and date_diff(data_end_date,a.DMND_DTE,day) <=60 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_child_sls_prev_2mnt,
        ifnull(sum((case when a.dma_nbr = 26 and date_diff(data_end_date,a.DMND_DTE,day) <=90 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_child_sls_prev_3mnt,
        ifnull(sum((case when a.dma_nbr = 26 and date_diff(data_end_date,a.DMND_DTE,day) <=180 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_child_sls_prev_6mnt,

        -- Beauty
        ifnull(sum((case when a.dma_nbr = 32 and date_diff(data_end_date,a.DMND_DTE,day) <=30 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_beauty_sls_prev_1mnt,
        ifnull(sum((case when a.dma_nbr = 32 and date_diff(data_end_date,a.DMND_DTE,day) <=60 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_beauty_sls_prev_2mnt,
        ifnull(sum((case when a.dma_nbr = 32 and date_diff(data_end_date,a.DMND_DTE,day) <=90 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_beauty_sls_prev_3mnt,
        ifnull(sum((case when a.dma_nbr = 32 and date_diff(data_end_date,a.DMND_DTE,day) <=180 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_beauty_sls_prev_6mnt,

        -- Acc/Jewel/Footwear
        ifnull(sum((case when a.dma_nbr = 44 and date_diff(data_end_date,a.DMND_DTE,day) <=30 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_acc_jew_foot_sls_prev_1mnt,
        ifnull(sum((case when a.dma_nbr = 44 and date_diff(data_end_date,a.DMND_DTE,day) <=60 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_acc_jew_foot_sls_prev_2mnt,
        ifnull(sum((case when a.dma_nbr = 44 and date_diff(data_end_date,a.DMND_DTE,day) <=90 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_acc_jew_foot_sls_prev_3mnt,
        ifnull(sum((case when a.dma_nbr = 44 and date_diff(data_end_date,a.DMND_DTE,day) <=180 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_acc_jew_foot_sls_prev_6mnt,

        -- Disc/Impluse
        ifnull(sum((case when a.dma_nbr = 45 and date_diff(data_end_date,a.DMND_DTE,day) <=30 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_disc_impul_sls_prev_1mnt,
        ifnull(sum((case when a.dma_nbr = 45 and date_diff(data_end_date,a.DMND_DTE,day) <=60 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_disc_impul_sls_prev_2mnt,
        ifnull(sum((case when a.dma_nbr = 45 and date_diff(data_end_date,a.DMND_DTE,day) <=90 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_disc_impul_sls_prev_3mnt,
        ifnull(sum((case when a.dma_nbr = 45 and date_diff(data_end_date,a.DMND_DTE,day) <=180 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_disc_impul_sls_prev_6mnt,

        -- Women's
        ifnull(sum((case when a.dma_nbr = 62 and date_diff(data_end_date,a.DMND_DTE,day) <=30 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_womens_sls_prev_1mnt,
        ifnull(sum((case when a.dma_nbr = 62 and date_diff(data_end_date,a.DMND_DTE,day) <=60 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_womens_sls_prev_2mnt,
        ifnull(sum((case when a.dma_nbr = 62 and date_diff(data_end_date,a.DMND_DTE,day) <=90 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_womens_sls_prev_3mnt,
        ifnull(sum((case when a.dma_nbr = 62 and date_diff(data_end_date,a.DMND_DTE,day) <=180 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_womens_sls_prev_6mnt,

        -- Young women's
        ifnull(sum((case when a.dma_nbr = 63 and date_diff(data_end_date,a.DMND_DTE,day) <=30 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_ywomens_sls_prev_1mnt,
        ifnull(sum((case when a.dma_nbr = 63 and date_diff(data_end_date,a.DMND_DTE,day) <=60 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_ywomens_sls_prev_2mnt,
        ifnull(sum((case when a.dma_nbr = 63 and date_diff(data_end_date,a.DMND_DTE,day) <=90 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_ywomens_sls_prev_3mnt,
        ifnull(sum((case when a.dma_nbr = 63 and date_diff(data_end_date,a.DMND_DTE,day) <=180 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_ywomens_sls_prev_6mnt,

        -- Outlet
        ifnull(sum((case when a.dma_nbr = 70 and date_diff(data_end_date,a.DMND_DTE,day) <=30 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_outlet_sls_prev_1mnt,
        ifnull(sum((case when a.dma_nbr = 70 and date_diff(data_end_date,a.DMND_DTE,day) <=60 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_outlet_sls_prev_2mnt,
        ifnull(sum((case when a.dma_nbr = 70 and date_diff(data_end_date,a.DMND_DTE,day) <=90 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_outlet_sls_prev_3mnt,
        ifnull(sum((case when a.dma_nbr = 70 and date_diff(data_end_date,a.DMND_DTE,day) <=180 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_outlet_sls_prev_6mnt,

        -- Active
        ifnull(sum((case when a.dma_nbr = 88 and date_diff(data_end_date,a.DMND_DTE,day) <=30 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_active_sls_prev_1mnt,
        ifnull(sum((case when a.dma_nbr = 88 and date_diff(data_end_date,a.DMND_DTE,day) <=60 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_active_sls_prev_2mnt,
        ifnull(sum((case when a.dma_nbr = 88 and date_diff(data_end_date,a.DMND_DTE,day) <=90 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_active_sls_prev_3mnt,
        ifnull(sum((case when a.dma_nbr = 88 and date_diff(data_end_date,a.DMND_DTE,day) <=180 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_active_sls_prev_6mnt,

        -- Misc
        ifnull(sum((case when a.dma_nbr = 99 and date_diff(data_end_date,a.DMND_DTE,day) <=30 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_misc_sls_prev_1mnt,
        ifnull(sum((case when a.dma_nbr = 99 and date_diff(data_end_date,a.DMND_DTE,day) <=60 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_misc_sls_prev_2mnt,
        ifnull(sum((case when a.dma_nbr = 99 and date_diff(data_end_date,a.DMND_DTE,day) <=90 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_misc_sls_prev_3mnt,
        ifnull(sum((case when a.dma_nbr = 99 and date_diff(data_end_date,a.DMND_DTE,day) <=180 then a.DMND_NET_CHRGD_AMT end )),0) as tot_dma_misc_sls_prev_6mnt,

    ifnull(SUM(a.DMND_MKTG_MKDN_AMT),0) as MMD, --tot marketing markdown
    ifnull(SUM(a.DMND_PLU_MKDN_AMT),0) as PLU, --total price look up
    ifnull(SUM(a.dmnd_kcsh_ernd_amt),0) as KCASH_EARN -- K Cash earned during transactions

  from `kohls-bda-prd.dp_marketing.bqt_sls_cust_agg` a
  right join `kohls-bda-mkt-prd.aa_sandbox.pst_uplift_dec22_pyd_cust_list` b
  on cast(a.cust_id as int) = cast(b.cust_id as int)

  where a.sls_typ_cde = 'S'
  and a.cust_id is not null
  and a.DMND_DTE between data_start_date and data_end_date
  group by 1;
-- 1,862,804 records


/************************************ Omni vs Store Data : OOT Sampling ************************************/
create or replace table `kohls-bda-mkt-prd.aa_sandbox.ovs_oot_cust_merged_33p_samp`
as
select * from `kohls-bda-mkt-prd.aa_sandbox.ovs_oot_cust_merged`
where total_12m_sls > 0
and mod(cust_id,3) = 0;

select count(distinct cust_id) from `kohls-bda-mkt-prd.aa_sandbox.ovs_oot_cust_merged_33p_samp`;
select sum(out_model1),sum(out_model2) from `kohls-bda-mkt-prd.aa_sandbox.ovs_oot_cust_merged_33p_samp`;


-- Select the dates
DECLARE cpgn_start_date,cpgn_end_date,vantage_date,data_end_date,data_start_date DATE;

-- June Test Customer List
SET (cpgn_start_date,cpgn_end_date,vantage_date,data_end_date,data_start_date) = (select AS STRUCT cpgn_start_date,cpgn_end_date,vantage_date,data_end_date,data_start_date
 from `kohls-bda-mkt-prd.aa_sandbox.pst_uplift_cpgn_dates_uplift`
 where cpgn = 'Dec-22 $10 PYD Gift');

-- Customers in latest CE test
create or replace table `kohls-bda-mkt-prd.aa_sandbox.pst_uplift_dec22_pyd_cust_ce_discounts`
OPTIONS (expiration_timestamp = TIMESTAMP_ADD(CURRENT_TIMESTAMP(), INTERVAL 24 HOUR))
  as
  select distinct a.cust_id, ifnull(cast(substr(b.discount,0,2) as int),0)/100 as ce_disc
  from `kohls-bda-mkt-prd.aa_sandbox.pst_uplift_dec22_pyd_cust_list` a
  left join `kohls-bda-mkt-prd.aa_sandbox.V3_data_beta` b
    on cast(a.cust_id as string) = cast(b.cust_id as string)
      and b.event_start_date between cpgn_start_date and cpgn_end_date
      and b.cust_id is not null
  order by ce_disc asc;

-- select count(*) from `kohls-bda-mkt-prd.aa_sandbox.ovs_train_str_cust_demog_pit_ce_discounts`;

-- QC
select
count(distinct case when ce_disc=0 then cust_id else null end) as cnt0,
count(distinct case when ce_disc!=0 then cust_id else null end) as cnt1,
 from `kohls-bda-mkt-prd.aa_sandbox.pst_uplift_dec22_pyd_cust_ce_discounts`;



-- Join all the tables
create or replace table `kohls-bda-mkt-prd.aa_sandbox.pst_uplift_dec22_pyd_cust_merged`
  as
  select
    distinct t1.*,
    t2.* except(cust_id),
    t3.* except(cust_id),
    t5.* except(cust_id),
    t6.* except(cust_id),
    t7.* except(cust_id),
    t8.* except(cust_id),
    t9.* except(cust_id)
  
  from `kohls-bda-mkt-prd.aa_sandbox.pst_uplift_dec22_pyd_cust_demog_pit` t1
  
  left join `kohls-bda-mkt-prd.aa_sandbox.pst_uplift_dec22_pyd_cust_demog_cfv` t2
    on t1.cust_id = t2.cust_id
  left join `kohls-bda-mkt-prd.aa_sandbox.pst_uplift_dec22_pyd_cust_tenures_acct_pit` t3
   on t1.cust_id = t3.cust_id
  left join `kohls-bda-mkt-prd.aa_sandbox.pst_uplift_dec22_pyd_cust_tst_12m` t5
    on t1.cust_id = t5.cust_id
  left join `kohls-bda-mkt-prd.aa_sandbox.pst_uplift_dec22_pyd_cust_kc_earns_redeems` t6
    on t1.cust_id = t6.cust_id
  left join `kohls-bda-mkt-prd.aa_sandbox.pst_uplift_dec22_pyd_cust_kr_earns_redeems` t7
    on t1.cust_id = t7.cust_id
  left join `kohls-bda-mkt-prd.aa_sandbox.pst_uplift_dec22_pyd_cust_list` t8
    on t1.cust_id = t8.cust_id
  left join `kohls-bda-mkt-prd.aa_sandbox.pst_uplift_dec22_pyd_cust_ce_discounts` t9
    on t1.cust_id = t9.cust_id
  order by t1.cust_id;
-- -- 8,170,808  records

