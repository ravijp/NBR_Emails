
create or replace table `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_v0` as
    select
        cust_id,
        fmale_ind,
        bst_cust_brth_dte,
        dmnd_dte,
        mstr_trn_hdr_id,
        sku_nbr,
        ecom_ind,
        dma_nm,
        dept_nm,
        a.dept_nbr,
        dma_nbr,
        mapp.dept_nbr_zid,
        ffld_net_chrgd_amt    --  SELECT count(*), count(distinct cust_id), count(distinct mstr_trn_hdr_id)
    FROM
        `kohls-bda-prd.dp_marketing.bqt_sls_cust_agg` a
    left join `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_dept_zid_mapping` as mapp
    on a.dept_nbr = mapp.dept_nbr

    WHERE
        dmnd_dte > "2021-06-30"  and dmnd_dte < "2022-08-01" --13 months of transactions
        and sls_typ_cde ='S'
        and cust_id in ( --july 2022 responders
            SELECT
                distinct cust_id
                FROM
                    `kohls-bda-prd.dp_marketing.bqt_sls_cust_agg`
                WHERE
                    dmnd_dte >= "2022-07-01"  and dmnd_dte < "2022-08-01"
                and sls_typ_cde ='S')
        and a.dept_nbr in --only selecting top 139 departments based up on 90% sales
        (543, 229, 610, 13, 743, 36, 121, 753, 571, 244, 63, 552, 235, 443, 111, 265, 230, 627, 89, 74, 852, 76, 52, 327, 613, 59, 239, 15, 311, 518, 139, 210, 11, 65, 352, 213, 153, 344, 135, 53, 45, 411, 418, 32, 66, 97, 357, 197, 44, 905, 115, 318, 147, 713, 266, 245, 46, 39, 25, 26, 944, 155, 152, 830, 117, 90, 656, 57, 242, 55, 12, 414, 355, 404, 853, 31, 343, 714, 144, 28, 315, 37, 211, 73, 41, 146, 251, 614, 231, 287, 100, 164, 91, 248, 744, 27, 456, 405, 60, 253, 43, 112, 133, 102, 81, 67, 544, 159, 371, 331, 71, 58, 56, 86, 228, 716, 339, 827, 219, 624, 33, 175, 452, 122, 94, 157, 347, 137, 35, 232, 611, 917, 444, 113, 156, 151, 75, 104, 964);


-- 
-- **********  Aggregating data at transactions level for each customer ********** 
-- 
create or replace table `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_v1` as
select
    cust_id,
    mstr_trn_hdr_id,
    dmnd_dte,
    array_agg(dept_nm ignore nulls) as dept_nms,
    array_agg(dept_nbr ignore nulls) as dept_nbrs,
    array_agg(dept_nbr_zid ignore nulls) as dept_nbr_zids,
    array_agg(ffld_net_chrgd_amt) as sale_prices,
    array_agg(sku_nbr) as skus
from `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_v0`
group by 1, 2, 3
order by dmnd_dte asc;


-- 
-- ********** Data Prep  ********** 
-- 

#standardSQL
create or replace table `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_v2` as
with main as (
    SELECT m.*REPLACE(ARRAY(SELECT DISTINCT el FROM m.dept_nbr_zids AS el) AS dept_nbr_zids), 
    ROW_NUMBER() OVER (PARTITION BY cust_id ORDER BY dmnd_dte DESC) AS rn 
    FROM `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_v1` m 
),     
max_deps as (
        select cust_id, 
            num_purchases, --total baskets
            ARRAY(SELECT DISTINCT x FROM UNNEST(concatenated) as x) as tot_cust_depts --List of purchased departments (cleaning the duplicate depts)
        from (
            select cust_id, ARRAY_CONCAT_AGG(dept_nbr_zids) as concatenated, count(distinct mstr_trn_hdr_id) as num_purchases
            FROM main
            GROUP BY cust_id
        )
)
-- , deps_mapp as (
--     select ARRAY(
--     SELECT dept_nbr_zid
--     FROM  `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_dept_zid_mapping`
--     ) as total_deps
-- )
select 
    main.cust_id, 
    mstr_trn_hdr_id, 
    dmnd_dte,
    dept_nbr_zids, 
    array_length(dept_nbr_zids) as total_basket_deps,
    array_length(max_deps.tot_cust_depts) as tot_cust_deps,
    -- max_deps.tot_cust_depts as cust_deps,
    max_deps.num_purchases,
    main.rn as tran_rnk
    -- ,
    -- array(
    --     select all_dep
    --     from unnest(total_deps) all_dep 
    --     left join unnest(max_deps.tot_cust_depts) pos_dep
    --     on pos_dep = all_dep
    --     where pos_dep is null
    --     ) neg_deps
from main --, deps_mapp
left join max_deps on max_deps.cust_id = main.cust_id
order by dmnd_dte
;




-- Master Data which we use to create sequences in IPython Notebook
-- **********  Data Filteration, assigning rnd_val column to do sampling ********** 
-- 

-- https://cloud.google.com/bigquery/docs/reference/standard-sql/arrays#building_arrays_of_arrays

create or replace table `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_v3` as
select cust_id, array_agg(struct(dept_nbr_zids) order by dmnd_dte) as dept_nbrs, RAND() as rnd_val
from `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_v2`
-- where tot_cust_deps > 3 and num_purchases > 1  -- 9871810 rows
where tot_cust_deps > 3 and num_purchases > 1 and tran_rnk < 21
group by 1;

-- 
-- **********  Creating a separate table for negative_samples(BPR Loss) ********** 
-- 
#standardSQL
create or replace table `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_v4` as
with main as (
    SELECT m.*REPLACE(ARRAY(SELECT DISTINCT el FROM m.dept_nbr_zids AS el) AS dept_nbr_zids), 
    ROW_NUMBER() OVER (PARTITION BY cust_id ORDER BY dmnd_dte DESC) AS rn 
    FROM `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_v1` m 
),     
max_deps as (
        select cust_id, num_purchases, ARRAY(SELECT DISTINCT x FROM UNNEST(concatenated) as x) as tot_cust_depts
        from (
            select cust_id, ARRAY_CONCAT_AGG(dept_nbr_zids) as concatenated, count(distinct mstr_trn_hdr_id) as num_purchases
            FROM main
            where rn !=1 and rn < 21
            GROUP BY 1
        )
), deps_mapp as (
    select ARRAY(
    SELECT dept_nbr_zid
    FROM  `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_dept_zid_mapping`
    ) as total_deps
)
select 
    max_deps.cust_id, 
    -- mstr_trn_hdr_id, 
    -- dept_nbr_zids, 
    -- array_length(dept_nbr_zids) as total_basket_deps,
    array_length(max_deps.tot_cust_depts) as tot_cust_deps,
    max_deps.tot_cust_depts as cust_deps,
    max_deps.num_purchases,
    array(
        select all_dep
        from unnest(deps_mapp.total_deps) all_dep 
        left join unnest(max_deps.tot_cust_depts) pos_dep
        on pos_dep = all_dep
        where pos_dep is null
        ) neg_deps
from max_deps, deps_mapp;
-- left join deps_mapp on max_deps.cust_id = deps_mapp.cust_id


---
---other departments population check
---
create or replace table `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_v0_other` as
    select
        cust_id,
        fmale_ind,
        bst_cust_brth_dte,
        dmnd_dte,
        mstr_trn_hdr_id,
        sku_nbr,
        ecom_ind,
        dma_nm, 
        dma_nbr,
        dept_nm,
        a.dept_nbr,
        maj_cl_nm,  
        maj_cl_nbr, 
        sub_cl_nm,  
        sub_cl_nbr,
        mapp.dept_nbr_zid,
        ffld_net_chrgd_amt,
        (case when a.dept_nbr in (543, 229, 610, 13, 743, 36, 121, 753, 571, 244, 63, 552, 235, 443, 111, 265, 230, 627, 89, 74, 852, 76, 52, 327, 613, 59, 239, 15, 311, 518, 139, 210, 11, 65, 352, 213, 153, 344, 135, 53, 45, 411, 418, 32, 66, 97, 357, 197, 44, 905, 115, 318, 147, 713, 266, 245, 46, 39, 25, 26, 944, 155, 152, 830, 117, 90, 656, 57, 242, 55, 12, 414, 355, 404, 853, 31, 343, 714, 144, 28, 315, 37, 211, 73, 41, 146, 251, 614, 231, 287, 100, 164, 91, 248, 744, 27, 456, 405, 60, 253, 43, 112, 133, 102, 81, 67, 544, 159, 371, 331, 71, 58, 56, 86, 228, 716, 339, 827, 219, 624, 33, 175, 452, 122, 94, 157, 347, 137, 35, 232, 611, 917, 444, 113, 156, 151, 75, 104, 964) then 1 else 0 end) as dept_flag,
        -- (case when cust_id in (SELECT
        --         distinct cust_id
        --         FROM
        --             `kohls-bda-prd.dp_marketing.bqt_sls_cust_agg`
        --         WHERE
        --             dmnd_dte >= "2022-07-01"  and dmnd_dte < "2022-08-01"
        --         and sls_typ_cde ='S') then 1 else 0 end) as july_resp_flag
    FROM
        `kohls-bda-prd.dp_marketing.bqt_sls_cust_agg` a
    left join `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_dept_zid_mapping` as mapp
    on a.dept_nbr = mapp.dept_nbr

    WHERE
        dmnd_dte > "2021-06-30"  and dmnd_dte < "2022-08-01" --13 months of transactions
        and sls_typ_cde ='S'
        and cust_id in ( --july 2022 responders
            SELECT
                distinct cust_id
                FROM
                    `kohls-bda-prd.dp_marketing.bqt_sls_cust_agg`
                WHERE
                    dmnd_dte >= "2022-07-01"  and dmnd_dte < "2022-08-01"
                and sls_typ_cde ='S')
;



-- 
-- how many customer have purchased from 139 + other departments
-- 
select cnt, count(distinct cust_id)
from
(SELECT cust_id, count(distinct dept_flag) as cnt FROM `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_v0_other` group by 1)
group by 1

cnt	f0_
1	6324868
2	7310175
total = 13635043

-- 
-- Checking product hierarchies https://docs.google.com/presentation/d/1gj7awIhEHHUaZwaFFI2TLHZg19Q3xyrGCnb7UwufgNc/edit#slide=id.p5
-- 
SELECT Count(DISTINCT cust_id) as cust_id,
       Count(DISTINCT dept_nbr) as dept_nbr,
       Count(DISTINCT dept_nm) as dept_nm,
       Count(DISTINCT maj_cl_nm) as maj_cl_nm,
       Count(DISTINCT maj_cl_nbr) as maj_cl_nbr,
       Count(DISTINCT sub_cl_nm) as sub_cl_nm,
       Count(DISTINCT sub_cl_nbr) as sub_cl_nbr,
       Count(DISTINCT styl_id) as styl_id,
       Count(DISTINCT sku_nbr) as sku_nbr
FROM   `kohls-bda-prd.dp_marketing.bqt_sls_cust_agg`
WHERE  dmnd_dte > "2021-06-30"
       AND dmnd_dte < "2022-08-01" --13 months of transactions
       AND sls_typ_cde = 'S'
       AND cust_id IN (--july 2022 responders
                      SELECT DISTINCT cust_id
                       FROM   `kohls-bda-prd.dp_marketing.bqt_sls_cust_agg`
                       WHERE  dmnd_dte >= "2022-07-01"
                              AND dmnd_dte < "2022-08-01"
                              AND sls_typ_cde = 'S') ;

cust_id	    dept_nbr	dept_nm	    maj_cl_nm	maj_cl_nbr	sub_cl_nm	sub_cl_nbr	styl_id	sku_nbr
13635043	364	        369	        1802    	9	        7262    	90      	864994	2764814

SELECT distinct cust_id,
        maj_cl_nm,
        dept_nbr,
        sub_cl_nbr,
        styl_id,
        sku_nbr,
        maj_cl_nbr
FROM   `kohls-bda-prd.dp_marketing.bqt_sls_cust_agg`
WHERE  dmnd_dte > "2021-06-30"
       AND dmnd_dte < "2022-08-01" --13 months of transactions
       AND sls_typ_cde = 'S'
       AND cust_id IN (--july 2022 responders
                      SELECT DISTINCT cust_id
                       FROM   `kohls-bda-prd.dp_marketing.bqt_sls_cust_agg`
                       WHERE  dmnd_dte >= "2022-07-01"
                              AND dmnd_dte < "2022-08-01"
                              AND sls_typ_cde = 'S') ;


--====major class level analysis
SELECT  
-- dma_nm, dept_nm, maj_cl_nm, count(*), count(distinct cust_id)
dma_nbr, dept_nbr, maj_cl_nbr, count(*), count(distinct cust_id)
FROM `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_v0_other` 
group by 1, 2, 3;


create or replace table `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_v0_other` as
    select
        cust_id,
        fmale_ind,
        bst_cust_brth_dte,
        mstr_trn_hdr_id,
        dmnd_dte,
        ffld_net_chrgd_amt,
        sku_nbr,
        ecom_ind,
        dma_nm, 
        dma_nbr,
        dept_nm,
        a.dept_nbr,
        maj_cl_nm,  
        maj_cl_nbr, 
        sub_cl_nm,  
        sub_cl_nbr,
        mapp.dept_nbr_zid,
        mapp.dept_flag
    FROM
        `kohls-bda-prd.dp_marketing.bqt_sls_cust_agg` a
    left join (select *, (case when dept_nbr in (543, 229, 610, 13, 743, 36, 121, 753, 571, 244, 63, 552, 235, 443, 111, 265, 230, 627, 89, 74, 852, 76, 52, 327, 613, 59, 239, 15, 311, 518, 139, 210, 11, 65, 352, 213, 153, 344, 135, 53, 45, 411, 418, 32, 66, 97, 357, 197, 44, 905, 115, 318, 147, 713, 266, 245, 46, 39, 25, 26, 944, 155, 152, 830, 117, 90, 656, 57, 242, 55, 12, 414, 355, 404, 853, 31, 343, 714, 144, 28, 315, 37, 211, 73, 41, 146, 251, 614, 231, 287, 100, 164, 91, 248, 744, 27, 456, 405, 60, 253, 43, 112, 133, 102, 81, 67, 544, 159, 371, 331, 71, 58, 56, 86, 228, 716, 339, 827, 219, 624, 33, 175, 452, 122, 94, 157, 347, 137, 35, 232, 611, 917, 444, 113, 156, 151, 75, 104, 964) then 1 else 0 end) as dept_flag from `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_dept_zid_mapping` )as mapp
    on a.dept_nbr = mapp.dept_nbr
    WHERE
        dmnd_dte > "2021-06-30"  and dmnd_dte < "2022-08-01" --13 months of transactions
        and sls_typ_cde ='S'
        and cust_id in ( --july 2022 responders
            SELECT
                distinct cust_id
                FROM
                    `kohls-bda-prd.dp_marketing.bqt_sls_cust_agg`
                WHERE
                    dmnd_dte >= "2022-07-01"  and dmnd_dte < "2022-08-01"
                and sls_typ_cde ='S')
;

