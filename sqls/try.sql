-- Nearly ~14mn responders in july 2022 month
-- Select 1% cust_ids at random


SELECT
    count(distinct cust_id), count(*)
FROM
    `kohls-bda-prd.dp_marketing.bqt_sls_cust_agg`
WHERE
    dmnd_dte >= "2022-07-01"  and dmnd_dte < "2022-08-01"


-- 
-- for a month, select all responders (cust_ids who make purchase) August 2022
-- for each responder, pull data till [last_trn_dte > July 2021]
-- columns selected for now =>
    -- at items level -> sku_nbr, dma_nm, dept_nm, ecom_ind, ffld_net_chrgd_amt
    -- unique at transactions level -> fmale_ind, bst_cust_brth_dte, dmnd_dte, mstr_trn_hdr_id 
    -- time column -> dmnd_dte
    -- sales transaction check -> sls_typ_cde ='S'
-- 

-- ********** checking responder count for all sales transactions ********** 

-- SELECT
--     count(distinct cust_id), count(*)
-- FROM
--     `kohls-bda-prd.dp_marketing.bqt_sls_cust_agg`
-- WHERE
--     dmnd_dte >= "2022-07-01"  and dmnd_dte < "2022-08-01"
--   and sls_typ_cde ='S'

-- f0_	f1_
-- 13637815	89793281

-- ********** checking responder count + age + gender for all sales transactions ********** 
    -- select count(distinct cust_id), count(*) from 
    -- (
    --   SELECT
    --     cust_id, fmale_ind, max(bst_cust_brth_dte) as bday_dte
    --   FROM
    --       `kohls-bda-prd.dp_marketing.bqt_sls_cust_agg`
    --   WHERE
    --       dmnd_dte >= "2022-07-01"  and dmnd_dte < "2022-08-01"
    --     and sls_typ_cde ='S'

    --   group by 1, 2 
    -- )
    -- f0_	f1_
    -- 13637815	13640068


-- ********** some cust_id have both 'Y' & 'N' as fmale_ind ********** 
    -- select count(*), count(distinct cust_id) from
    -- (
    --   SELECT
    --     cust_id, count(distinct fmale_ind) as gen_cnt, count(distinct bst_cust_brth_dte) as bday_dte_cnt
    --   FROM
    --       `kohls-bda-prd.dp_marketing.bqt_sls_cust_agg`
    --   WHERE
    --       dmnd_dte >= "2022-07-01"  and dmnd_dte < "2022-08-01"
    --     and sls_typ_cde ='S'

    --   group by 1
    -- )
    -- where gen_cnt>1
    -- f0_	f1_
    -- 277	277

-- 
-- **********  First View of raw table with set of columns ********** 
-- 
create or replace table `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_v0` as
    select
        cust_id,
        fmale_ind,
        bst_cust_brth_dte,
        dmnd_dte,
        mstr_trn_hdr_id,
        sku_nbr,
        ecom_ind,
        dma_nm,
        dept_nm,
        a.dept_nbr,
        dma_nbr,
        mapp.dept_nbr_zid,
        ffld_net_chrgd_amt    --  SELECT count(*), count(distinct cust_id), count(distinct mstr_trn_hdr_id)
    FROM
        `kohls-bda-prd.dp_marketing.bqt_sls_cust_agg` a
    left join `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_dept_zid_mapping` as mapp
    on a.dept_nbr = mapp.dept_nbr

    WHERE
        dmnd_dte > "2021-06-30"  and dmnd_dte < "2022-08-01"
        and sls_typ_cde ='S'
        and cust_id in (
            SELECT
                distinct cust_id
                FROM
                    `kohls-bda-prd.dp_marketing.bqt_sls_cust_agg`
                WHERE
                    dmnd_dte >= "2022-07-01"  and dmnd_dte < "2022-08-01"
                and sls_typ_cde ='S')
        and a.dept_nbr in (543, 229, 610, 13, 743, 36, 121, 753, 571, 244, 63, 552, 235, 443, 111, 265, 230, 627, 89, 74, 852, 76, 52, 327, 613, 59, 239, 15, 311, 518, 139, 210, 11, 65, 352, 213, 153, 344, 135, 53, 45, 411, 418, 32, 66, 97, 357, 197, 44, 905, 115, 318, 147, 713, 266, 245, 46, 39, 25, 26, 944, 155, 152, 830, 117, 90, 656, 57, 242, 55, 12, 414, 355, 404, 853, 31, 343, 714, 144, 28, 315, 37, 211, 73, 41, 146, 251, 614, 231, 287, 100, 164, 91, 248, 744, 27, 456, 405, 60, 253, 43, 112, 133, 102, 81, 67, 544, 159, 371, 331, 71, 58, 56, 86, 228, 716, 339, 827, 219, 624, 33, 175, 452, 122, 94, 157, 347, 137, 35, 232, 611, 917, 444, 113, 156, 151, 75, 104, 964);


SELECT count(*), count(distinct cust_id), count(distinct mstr_trn_hdr_id)
from `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_v0`;
--with dept_nbr condition
    -- f0_	f1_	f2_
    -- 511443760	12947644	134969662
    --    486822292
--- with out dept_nbr condition
    -- f0_	f1_	f2_
    -- 522213721	13637815	138459086
    -- 138459162 

-- 
-- **********  Aggregating arrays at customer x transactions level ********** 
-- 
create or replace table `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_v1` as
select
    cust_id,
    mstr_trn_hdr_id,
    dmnd_dte,
    array_agg(dept_nm ignore nulls) as dept_nms,
    array_agg(dept_nbr ignore nulls) as dept_nbrs,
    array_agg(dept_nbr_zid ignore nulls) as dept_nbr_zids,
    array_agg(ffld_net_chrgd_amt) as sale_prices,
    array_agg(sku_nbr) as skus
from `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_v0`
-- where cust_id = 18699390
group by 1, 2, 3
order by dmnd_dte asc;

SELECT count(*), count(distinct cust_id), count(distinct mstr_trn_hdr_id)
from `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_v1`;
-- after dept_nbr correction in v0
    -- f0_	f1_	f2_
    -- 131928649	13319562	131928611
--with ecom_ind in group by
    -- f0_	f1_	f2_ 
    -- 134969734	12947644	134969662

--with current code (3 group by vars)
    -- f0_	f1_	f2_
    -- 134969701	12947644	134969662

SELECT count(distinct cust_id),count(distinct mstr_trn_hdr_id)  FROM `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_v1` 
-- after dept_nbr correction in v0
    -- f0_	f1_
    -- 13319562	131928611

-- f0_	f1_
-- 13637815	138459086

-- 
-- **********  152 transactions having multiple ecom_ind ********** 
-- 
select * 
FROM `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_v1`
where mstr_trn_hdr_id in (
select mstr_trn_hdr_id from(
SELECT  mstr_trn_hdr_id, count(*) as cnt  FROM `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_v1` group by 1) where cnt >1)




with main as (select
        cust_id,
        fmale_ind,
        bst_cust_brth_dte,
        dmnd_dte,
        mstr_trn_hdr_id,
        sku_nbr,
        ecom_ind,
        dma_nm,
        dept_nm,
        dept_nbr,
        dma_nbr,
        ffld_net_chrgd_amt    --  SELECT count(*), count(distinct cust_id), count(distinct mstr_trn_hdr_id)
    FROM
        `kohls-bda-prd.dp_marketing.bqt_sls_cust_agg`
    WHERE
        dmnd_dte > "2021-06-30"  and dmnd_dte < "2022-08-01"
        and sls_typ_cde ='S'
        and cust_id in (
        SELECT
            distinct cust_id
            FROM
                `kohls-bda-prd.dp_marketing.bqt_sls_cust_agg`
            WHERE
                dmnd_dte >= "2022-07-01"  and dmnd_dte < "2022-08-01"
            and sls_typ_cde ='S'
            and dept_nbr in (543, 229, 610, 13, 743, 36, 121, 753, 571, 244, 63, 552, 235, 443, 111, 265, 230, 627, 89, 74, 852, 76, 52, 327, 613, 59, 239, 15, 311, 518, 139, 210, 11, 65, 352, 213, 153, 344, 135, 53, 45, 411, 418, 32, 66, 97, 357, 197, 44, 905, 115, 318, 147, 713, 266, 245, 46, 39, 25, 26, 944, 155, 152, 830, 117, 90, 656, 57, 242, 55, 12, 414, 355, 404, 853, 31, 343, 714, 144, 28, 315, 37, 211, 73, 41, 146, 251, 614, 231, 287, 100, 164, 91, 248, 744, 27, 456, 405, 60, 253, 43, 112, 133, 102, 81, 67, 544, 159, 371, 331, 71, 58, 56, 86, 228, 716, 339, 827, 219, 624, 33, 175, 452, 122, 94, 157, 347, 137, 35, 232, 611, 917, 444, 113, 156, 151, 75, 104, 964)
        ))
select cust_id,
    mstr_trn_hdr_id,
    dmnd_dte,
    array_agg(dept_nm ignore nulls) as dept_nms,
    array_agg(dept_nbr ignore nulls) as dept_nbrs,
    array_agg(ffld_net_chrgd_amt) as sale_prices,
    array_agg(sku_nbr) as skus
from main
-- where cust_id = 18699390
group by 1, 2, 3
order by dmnd_dte asc








-- 
-- to get total unique departments at cust_id level
-- 

#standardSQL
with main as (
    SELECT * REPLACE(ARRAY(SELECT DISTINCT el FROM t.dept_nbr_zids AS el) AS dept_nbr_zids)
    FROM `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_v1` t
    )
select cust_id, ARRAY(SELECT DISTINCT x FROM UNNEST(concatenated) as x) as tot_cust_depts
from (
    select cust_id, ARRAY_CONCAT_AGG(dept_nbr_zids) as concatenated
    FROM main
    GROUP BY cust_id
)

#standardSQL
with main as (SELECT * REPLACE(ARRAY(SELECT DISTINCT el FROM t.dept_nbr_zids AS el) AS dept_nbr_zids)
FROM `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_v1` t   ), 
max_deps as (
    select cust_id, ARRAY(SELECT DISTINCT x FROM UNNEST(concatenated) as x) as tot_cust_depts
    from (
        select cust_id, ARRAY_CONCAT_AGG(dept_nbr_zids) as concatenated
        FROM main
        GROUP BY cust_id
    )
)
select 
    main.cust_id, 
    mstr_trn_hdr_id, 
    dept_nbr_zids, 
    array_length(dept_nbr_zids) as total_depts,
    max_deps.tot_cust_depts
from main
left join max_deps on max_deps.cust_id = main.cust_id
-- 
-- v2
-- 



create or replace table `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_v0` as
    select
        cust_id,
        fmale_ind,
        bst_cust_brth_dte,
        dmnd_dte,
        mstr_trn_hdr_id,
        sku_nbr,
        ecom_ind,
        dma_nm,
        dept_nm,
        a.dept_nbr,
        dma_nbr,
        mapp.dept_nbr_zid,
        ffld_net_chrgd_amt    --  SELECT count(*), count(distinct cust_id), count(distinct mstr_trn_hdr_id)
    FROM
        `kohls-bda-prd.dp_marketing.bqt_sls_cust_agg` a
    left join `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_dept_zid_mapping` as mapp
    on a.dept_nbr = mapp.dept_nbr

    WHERE
        dmnd_dte > "2021-06-30"  and dmnd_dte < "2022-08-01"
        and sls_typ_cde ='S'
        and cust_id in (
            SELECT
                distinct cust_id
                FROM
                    `kohls-bda-prd.dp_marketing.bqt_sls_cust_agg`
                WHERE
                    dmnd_dte >= "2022-07-01"  and dmnd_dte < "2022-08-01"
                and sls_typ_cde ='S')
        and a.dept_nbr in (543, 229, 610, 13, 743, 36, 121, 753, 571, 244, 63, 552, 235, 443, 111, 265, 230, 627, 89, 74, 852, 76, 52, 327, 613, 59, 239, 15, 311, 518, 139, 210, 11, 65, 352, 213, 153, 344, 135, 53, 45, 411, 418, 32, 66, 97, 357, 197, 44, 905, 115, 318, 147, 713, 266, 245, 46, 39, 25, 26, 944, 155, 152, 830, 117, 90, 656, 57, 242, 55, 12, 414, 355, 404, 853, 31, 343, 714, 144, 28, 315, 37, 211, 73, 41, 146, 251, 614, 231, 287, 100, 164, 91, 248, 744, 27, 456, 405, 60, 253, 43, 112, 133, 102, 81, 67, 544, 159, 371, 331, 71, 58, 56, 86, 228, 716, 339, 827, 219, 624, 33, 175, 452, 122, 94, 157, 347, 137, 35, 232, 611, 917, 444, 113, 156, 151, 75, 104, 964);


SELECT count(*), count(distinct cust_id), count(distinct mstr_trn_hdr_id)
from `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_v0`;
--with dept_nbr condition
    -- f0_	f1_	f2_
    -- 511443760	12947644	134969662
    --    486822292
--- with out dept_nbr condition
    -- f0_	f1_	f2_
    -- 522213721	13637815	138459086
    -- 138459162 

-- 
-- **********  Aggregating arrays at customer x transactions level ********** 
-- 
create or replace table `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_v1` as
select
    cust_id,
    mstr_trn_hdr_id,
    dmnd_dte,
    array_agg(dept_nm ignore nulls) as dept_nms,
    array_agg(dept_nbr ignore nulls) as dept_nbrs,
    array_agg(dept_nbr_zid ignore nulls) as dept_nbr_zids,
    array_agg(ffld_net_chrgd_amt) as sale_prices,
    array_agg(sku_nbr) as skus
from `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_v0`
-- where cust_id = 18699390
group by 1, 2, 3
order by dmnd_dte asc;


#standardSQL
create or replace table `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_v2` as
with main as (
    SELECT * REPLACE(ARRAY(SELECT DISTINCT el FROM t.dept_nbr_zids AS el) AS dept_nbr_zids)
    FROM `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_v1` t   
),     
max_deps as (
        select cust_id, num_purchases, ARRAY(SELECT DISTINCT x FROM UNNEST(concatenated) as x) as tot_cust_depts
        from (
            select cust_id, ARRAY_CONCAT_AGG(dept_nbr_zids) as concatenated, count(distinct mstr_trn_hdr_id) as num_purchases
            FROM main
            GROUP BY cust_id
        )
), deps_mapp as (
    select ARRAY(
    SELECT dept_nbr_zid
    FROM  `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_dept_zid_mapping`
    ) as total_deps
)
select 
    main.cust_id, 
    mstr_trn_hdr_id, 
    dept_nbr_zids, 
    array_length(dept_nbr_zids) as total_basket_deps,
    array_length(max_deps.tot_cust_depts) as tot_cust_deps,
    max_deps.tot_cust_depts as cust_deps,
    max_deps.num_purchases,
    array(
        select all_dep
        from unnest(total_deps) all_dep 
        left join unnest(max_deps.tot_cust_depts) pos_dep
        on pos_dep = all_dep
        where pos_dep is null
        ) neg_deps
from main, deps_mapp
left join max_deps on max_deps.cust_id = main.cust_id



-- 
-- 
-- 
-- create or replace table `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_v3` as
-- -- SELECT distinct cust_id,tot_cust_deps, num_purchases FROM `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_v2`
-- select cust_id, neg_deps, array_agg(struct(dept_nbr_zids)) as dept_nbrs, array_reverse(dept_nbr_zids)[offset[0]] as last_bucket, RAND() as rnd_val
-- from `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_v2`
-- where tot_cust_deps > 3 and num_purchases > 1
-- -- WHERE RAND() < 0.1
-- group by 1, 2

-- https://cloud.google.com/bigquery/docs/reference/standard-sql/arrays#building_arrays_of_arrays

create or replace table `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_v3` as
-- SELECT distinct cust_id,tot_cust_deps, num_purchases FROM `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_v2`
select cust_id, array_agg(struct(dept_nbr_zids)) as dept_nbrs, RAND() as rnd_val
from `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_v2`
where tot_cust_deps > 3 and num_purchases > 1
-- WHERE RAND() < 0.1
group by 1





#standardSQL
create or replace table `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_v4` as
with main as (
    SELECT * REPLACE(ARRAY(SELECT DISTINCT el FROM t.dept_nbr_zids AS el) AS dept_nbr_zids)
    FROM `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_v1` t   
),     
max_deps as (
        select cust_id, num_purchases, ARRAY(SELECT DISTINCT x FROM UNNEST(concatenated) as x) as tot_cust_depts
        from (
            select cust_id, ARRAY_CONCAT_AGG(dept_nbr_zids) as concatenated, count(distinct mstr_trn_hdr_id) as num_purchases
            FROM main
            GROUP BY cust_id
        )
), deps_mapp as (
    select ARRAY(
    SELECT dept_nbr_zid
    FROM  `kohls-bda-mkt-prd.dp_marketing_sandbox.email_recomm_dept_zid_mapping`
    ) as total_deps
)
select 
    max_deps.cust_id, 
    -- mstr_trn_hdr_id, 
    -- dept_nbr_zids, 
    -- array_length(dept_nbr_zids) as total_basket_deps,
    array_length(max_deps.tot_cust_depts) as tot_cust_deps,
    max_deps.tot_cust_depts as cust_deps,
    max_deps.num_purchases,
    array(
        select all_dep
        from unnest(deps_mapp.total_deps) all_dep 
        left join unnest(max_deps.tot_cust_depts) pos_dep
        on pos_dep = all_dep
        where pos_dep is null
        ) neg_deps
from max_deps, deps_mapp
-- left join deps_mapp on max_deps.cust_id = deps_mapp.cust_id

